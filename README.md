# io-oidis-nodejsre

> Runtime environment for back-end projects based on custom Node.js build.

## Requirements

This library does not have any special requirements, but it depends on the 
[WUI Builder](https://gitlab.com/oidis/io-oidis-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Project integration tests

Integration tests are located in [test/integration/javascript/Io/Oidis/NodejsRE](test/integration/javascript/Io/Oidis/NodejsRE) directory.
You can execute them with commands listed below from project root directory. Asterisk inside command path could be replaced by one
of mac, linux, or win platform keys, but it works properly if only one folder is matching this globbing pattern. Do not forget to add .exe 
extension for node binary on Windows OS.
```shell
# filter required test suite by commenting line in integration index.js file
./build/releases/app-*-gyp/target/node test/integration/javascript/Io/Oidis/NodejsRE/index.js

# Windows
build\releases\app-win-gyp\target\node.exe test\integration\javascript\Io\Oidis\NodejsRE\index.js
```

## History

### v2022.1.0
Fixed handling of readFileSync and some other minor fixes in resources manager. Updated solutions for arm archs. Fixed support for http proxy handling.
### v2021.2.0
Added ability to detect attached debugger. Fixed node arguments usage in embedded and static mode. Refactored tests and added tests for CLI 
arguments processor. Secured http/https backdoor. Updated JS entrypoint in node loader.
### v2021.0.0
The Node.Js core has been upgraded to 14.15 LTS. Simplified and generalized guiProxy config value parser.
### v2020.1.0
Updated integration tests and fixed some minor issues in resource_manager. Proper handling of options for read file.
### v2020.0.0
Refactored resource reader/writer API into ResourceManager class. Temporary disabled GUI callbacks due to segfaults from 
cross-thread access to v8::Isolate. Renamed internal resources_loader into resource_manager.
### v2019.1.0
Usage of resource writer instead of simple stream-based implementation.
### v2019.0.2
Libelf replaced by stream-based resources. Added full support for OSX. Added ability to pass externally created resources map.
### v2019.0.1
Added support for cross-compilation to aarch64. Upgrade of NodeJS to 10.15 LTS. Updated configuration to produce node as shared library.
Integrated http/https redirection to enable patching feature for native node module build based on NodejsRE distribution
instead of official one (change from node.lib=>node.exe to node.lib=>node.dll).
### v2019.0.0
Added more known file extensions. Added wrapper for fileRead with async API.
### v2018.3.0
Conversion of the project into the code based for custom Nodejs builds. Initial implementation of embedded resources loader.
### v1.1.0
Update of Nodejs version and tests. SCR update and change of history ordering.
### v1.0.1
Convert config files from XML to JSONP. Update of Commons lib.
### v1.0.0
Initial release.

## License

This software is owned or controlled by Oidis.
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright 2017-2019 [NXP](http://nxp.com/)
Copyright 2019-2022 [Oidis](https://www.oidis.org/)

/* ********************************************************************************************************* *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "env-inl.h"
#include <node_buffer.h>
#include <ResourceManager/ResourceManager.hpp>

#include "resource_manager.h"

namespace InternalResourceManager = Io::Oidis::Onion::ResourceManager;

static InternalResourceManager::ResourceManager resourceManager;

namespace node {
    using std::string;
    using v8::Context;
    using v8::FunctionCallbackInfo;
    using v8::HandleScope;
    using v8::Isolate;
    using v8::Local;
    using v8::Object;
    using v8::String;
    using v8::Value;

    void ResourceManager::Initialize(Local <Object> $target, Local <Value> $unused, Local <Context> $context, void* $priv) {
        SetMethod($context, $target, "read", Read);
        SetMethod($context, $target, "write", Write);
        SetMethod($context, $target, "initialize", InitializeManager);
        SetMethod($context, $target, "destroy", Destroy);
    }

    void ResourceManager::InitializeManager(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();
        Local <Context> context = isolate->GetCurrentContext();
        HandleScope scope(isolate);
        const std::string path = string(*String::Utf8Value(isolate, $args[0]));
        Local <v8::Function> logCallback;
        bool jsLogCallbackAvailable = false;

        if ($args.Length() == 2) {
            jsLogCallbackAvailable = true;
            logCallback = Local<v8::Function>::Cast($args[1]);
        }

        if (jsLogCallbackAvailable) {
            ResourceManager::setUpLoggingToJavascript(logCallback, isolate, context);
        } else {
            ResourceManager::disableLogging();
        }

        try {
            resourceManager.Initialize(path);
// TODO(mkelnar) check that manager is able to access resources, this action needs to read binary and prepare cache on POSIX sot it can take
//  some time.
//            if (InternalResourceManager::Reader::ResourceReader::getCachedResources().empty()) {
//                isolate->ThrowException(v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "Could not find any resources")));
//            }
        } catch (const std::exception &exception) {
            isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, exception.what()).ToLocalChecked()));
        } catch (...) {
            isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, "Unknown error").ToLocalChecked()));
        }
    }

    void ResourceManager::Destroy(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        (void)$args;
        resourceManager.Destroy();
    }

    void ResourceManager::Read(const FunctionCallbackInfo <Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();
        Local <Context> context = isolate->GetCurrentContext();
        HandleScope scope(isolate);
        bool jsLogCallbackAvailable = false;

        if ($args.Length() == 3 || $args.Length() == 4) {
            if ($args[0]->IsString() && $args[1]->IsString() && $args[2]->IsBoolean()) {
                Local <v8::Function> logCallback;
                const string group = string(*String::Utf8Value(isolate, $args[0]));
                const string id = string(*String::Utf8Value(isolate, $args[1]));
                const bool force = $args[2]->BooleanValue(isolate);

                if ($args.Length() == 4 && !$args[3]->IsUndefined() && !$args[3]->IsNull()) {
                    if (!$args[3]->IsFunction()) {
                        isolate->ThrowException(v8::Exception::TypeError(
                                v8::String::NewFromUtf8(isolate, "The optional 4th argument has to be a function").ToLocalChecked()));
                        return;
                    }
                    jsLogCallbackAvailable = true;
                    logCallback = Local<v8::Function>::Cast($args[3]);
                }

                try {
                    if (jsLogCallbackAvailable) {
                        ResourceManager::setUpLoggingToJavascript(logCallback, isolate, context);
                    } else {
                        ResourceManager::disableLogging();
                    }

                    if ((group + id).empty()) {
                        throw std::runtime_error("At least one of Group or ID parameters has to be not empty");
                    }
                    const auto resource = resourceManager.Read(group, id, force);

                    const auto rawData = resource.getData();
                    const auto size = rawData.size();
                    const char *bufPtr = reinterpret_cast<const char *>(&rawData[0]);
                    v8::Local <v8::Object> buf;

                    if (node::Buffer::Copy(isolate, bufPtr, size).ToLocal(&buf)) {
                        $args.GetReturnValue().Set(buf);
                    } else {
                        isolate->ThrowException(
                                v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "Cannot create buffer from resource").ToLocalChecked()));
                    }
                } catch (std::exception &ex) {
                    isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, ex.what()).ToLocalChecked()));
                } catch (...) {
                    isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, "Unknown error").ToLocalChecked()));
                }
            } else {
                isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(isolate, "Group and id have to be strings").ToLocalChecked()));
            }
        } else {
            isolate->ThrowException(v8::Exception::SyntaxError(
                    String::NewFromUtf8(isolate, "Incorrect count of arguments, 3 or 4 expected").ToLocalChecked()));
        }
    }

    void ResourceManager::Write(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();
        Local <Context> context = isolate->GetCurrentContext();
        HandleScope scope(isolate);
        bool jsLogCallbackAvailable = false;

        if ($args.Length() == 3 || $args.Length() == 4) {
            if ($args[0]->IsString() && $args[1]->IsString() && $args[2]->IsString()) {
                Local <v8::Function> logCallback;
                const string group = string(*String::Utf8Value(isolate, $args[0]));
                const string id = string(*String::Utf8Value(isolate, $args[1]));
                const string pathToResource = string(*String::Utf8Value(isolate, $args[2]));
                string resourceId = group + "/" + id;

                if ($args.Length() == 4 && !$args[3]->IsUndefined() && !$args[3]->IsNull()) {
                    if (!$args[3]->IsFunction()) {
                        isolate->ThrowException(v8::Exception::TypeError(
                                v8::String::NewFromUtf8(isolate, "The optional 4th argument has to be a function").ToLocalChecked()));
                        return;
                    }
                    jsLogCallbackAvailable = true;
                    logCallback = Local<v8::Function>::Cast($args[3]);
                }

                try {
                    if (jsLogCallbackAvailable) {
                        ResourceManager::setUpLoggingToJavascript(logCallback, isolate, context);
                    } else {
                        ResourceManager::disableLogging();
                    }
                    if ((group + id).empty() || pathToResource.empty()) {
                        throw std::runtime_error("At least one of Group or ID parameters and path to resources have to be not empty");
                    }

                    InternalResourceManager::Core::Resource resource{pathToResource};
                    resourceManager.Write({{std::move(resourceId), std::move(resource)}});
                } catch (const std::exception &exception) {
                    isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, exception.what()).ToLocalChecked()));
                } catch (...) {
                    isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, "Unknown error").ToLocalChecked()));
                }
            } else {
                isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(
                        isolate, "Group, ID, and path to resource have to be strings").ToLocalChecked()));
            }
        } else {
            isolate->ThrowException(v8::Exception::SyntaxError(String::NewFromUtf8(
                    isolate, "Incorrect count of arguments, 3 or 5 expected").ToLocalChecked()));
        }
    }

    bool ResourceManager::HasEmbeddedResources(const std::string &$path) {
        bool hasEmbeddedResources = false;

        try {
            ResourceManager::disableLogging();
            InternalResourceManager::Reader::ReaderSettings settings{$path};
            InternalResourceManager::Reader::ResourceReader resourceReader{std::move(settings)};
            hasEmbeddedResources = resourceReader.HasEmbeddedResources();
        } catch (...) {
            // suppress every exception and don't output anything (since the standard output and error are being parsed by the nodejs),
            // if something goes wrong, there are no embedded resources
        }

        return hasEmbeddedResources;
    }

    void ResourceManager::setUpLoggingToJavascript(v8::Local <v8::Function> $callback, v8::Isolate *$isolate,
                                                   v8::Local <v8::Context> &$context) {
        const auto invokeJsCallback = [$context, $isolate, $callback](std::string &&$message, std::string &&$verbosity) {
            Local <Value> argv[2];
            argv[0] = {v8::String::NewFromUtf8($isolate, $message.c_str(), v8::NewStringType::kNormal).ToLocalChecked()};
            argv[1] = {v8::String::NewFromUtf8($isolate, $verbosity.c_str(), v8::NewStringType::kNormal).ToLocalChecked()};
            $callback->Call($context, Null($isolate), 2, argv).ToLocalChecked();
        };

        auto loggerErrorCallback = [invokeJsCallback](std::string &&$message) {
            invokeJsCallback(std::move($message), "error");
        };

        auto loggerInfoCallback = [invokeJsCallback](std::string &&$message) {
            invokeJsCallback(std::move($message), "info");
        };

        auto loggerDebugCallback = [invokeJsCallback](std::string &&$message) {
            invokeJsCallback(std::move($message), "debug");
        };

        InternalResourceManager::Logger::Logger logger{std::move(loggerErrorCallback), std::move(loggerInfoCallback),
                                                       std::move(loggerDebugCallback)};
        InternalResourceManager::Logger::LogManager::setLogger(std::move(logger));
    }

    void ResourceManager::disableLogging() {
        InternalResourceManager::Logger::LogManager::setLogger(InternalResourceManager::Logger::LoggerDevNull{});
    }
}  // namespace node

NODE_MODULE_CONTEXT_AWARE_INTERNAL(resource_manager,
        node::ResourceManager::Initialize
)
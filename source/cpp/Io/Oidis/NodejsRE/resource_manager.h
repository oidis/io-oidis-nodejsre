/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2020 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef SRC_RESOURCE_MANAGER_H_
#define SRC_RESOURCE_MANAGER_H_

#if defined(NODE_WANT_INTERNALS) && NODE_WANT_INTERNALS

#include <string>

#include "node_internals.h"

namespace node {
    class ResourceManager {
     public:
        static void Initialize(v8::Local<v8::Object> $target, v8::Local<v8::Value> $unused, v8::Local<v8::Context> $context, void* $priv);

        /**
         * Arguments when called from the js:
         * path <string>, log js callback <function, optional>
         */
        static void InitializeManager(const v8::FunctionCallbackInfo<v8::Value> &$args);

        /**
         * Arguments when called from the js:
         * (none)
         */
        static void Destroy(const v8::FunctionCallbackInfo <v8::Value> &$args);

        /**
         * Arguments when called from the js:
         * group <string>, id <string>, force <boolean>, log js callback <function, optional>
         */
        static void Read(const v8::FunctionCallbackInfo <v8::Value> &$args);

        /**
         * Arguments when called from the js:
         * group <string>, id <string>, path-to-resource <string>, log js callback <function, optional>
         */
        static void Write(const v8::FunctionCallbackInfo <v8::Value> &$args);

        /**
         * Arguments when called from the js:
         * (none)
         */
        static bool HasEmbeddedResources(const std::string &$path);

     private:
        static void setUpLoggingToJavascript(v8::Local <v8::Function> $callback, v8::Isolate *$isolate, v8::Local<v8::Context> &$context);

        static void disableLogging();
    };
}  // namespace node

#endif  // defined(NODE_WANT_INTERNALS) && NODE_WANT_INTERNALS

#endif  // SRC_RESOURCE_MANAGER_H_

/* ********************************************************************************************************* *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <node_buffer.h>

#include "env-inl.h"
#include "guiProxy.h"

namespace node {
    using std::string;
    using v8::Context;
    using v8::FunctionCallbackInfo;
    using v8::Isolate;
    using v8::Local;
    using v8::Object;
    using v8::String;
    using v8::Value;
    using v8::Integer;
    using v8::Null;

    std::shared_ptr <Gui::SplashScreen::SplashScreenBase> GuiProxy::splashScreenInstance = nullptr;
    std::shared_ptr <Gui::NotifyIcon::NotifyIconBase> GuiProxy::notifyIconInstance = nullptr;
    std::shared_ptr <Gui::TaskBar::TaskBarBase> GuiProxy::taskBarInstance = nullptr;
    v8::Persistent <v8::Function> GuiProxy::splashScreenOnOpenCallback = {};
    v8::Persistent <v8::Function> GuiProxy::splashScreenOnCloseCallback = {};

    void GuiProxy::Initialize(Local <Object> $target, Local <Value> $unused, Local <Context> $context) {
        Environment *env = Environment::GetCurrent($context);
        env->SetMethod($target, "dummy", Dummy);
        env->SetMethod($target, "SplashScreenFactoryCreate", SplashScreenFactoryCreate);
        env->SetMethod($target, "SplashScreenShow", SplashScreenShow);
        env->SetMethod($target, "SplashScreenHide", SplashScreenHide);
        env->SetMethod($target, "SplashScreenSetOnOpenCB", SplashScreenSetOnOpenCB);
        env->SetMethod($target, "SplashScreenSetOnCloseCB", SplashScreenSetOnCloseCB);
        env->SetMethod($target, "SplashScreenAddProgressBar", SplashScreenAddProgressBar);
        env->SetMethod($target, "SplashScreenAddProgressLabel", SplashScreenAddProgressLabel);
        env->SetMethod($target, "SplashScreenSetProgressLabelValue", SplashScreenSetProgressLabelValue);
        env->SetMethod($target, "SplashScreenAddStatusLabel", SplashScreenAddStatusLabel);
        env->SetMethod($target, "SplashScreenSetStatusLabelValue", SplashScreenSetStatusLabelValue);
        env->SetMethod($target, "SplashScreenUpdate", SplashScreenUpdate);
        env->SetMethod($target, "SplashScreenSetProgressBarValue", SplashScreenSetProgressBarValue);
        env->SetMethod($target, "NotifyIconFactoryCreate", NotifyIconFactoryCreate);
        env->SetMethod($target, "TaskBarFactoryCreate", TaskBarFactoryCreate);
        env->SetMethod($target, "TaskBarSetProgressValue", TaskBarSetProgressValue);
        env->SetMethod($target, "TaskBarSetProgressState", TaskBarSetProgressState);
        env->SetMethod($target, "Finalize", Finalize);
    }

    void GuiProxy::Dummy(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        (void)Isolate::GetCurrent();
    }

    void GuiProxy::Finalize(const v8::FunctionCallbackInfo <v8::Value> &$args) {
//        GuiProxy::splashScreenInstance.reset();
        GuiProxy::notifyIconInstance.reset();
        GuiProxy::taskBarInstance.reset();
    }

    void GuiProxy::SplashScreenUpdate(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();

        if (!GuiProxy::splashScreenInstance) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "SplashScreen not created.").ToLocalChecked()));
            return;
        }

        GuiProxy::splashScreenInstance->Update();
    }

    void GuiProxy::SplashScreenFactoryCreate(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();
        Local <Context> context = isolate->GetCurrentContext();

        if ($args.Length() != 1) {
            isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(
                    isolate, "Missing argument.").ToLocalChecked()));
            return;
        }

        if (!$args[0]->IsObject()) {
            isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(
                    isolate, "Expected object argument.").ToLocalChecked()));
            return;
        }

        Gui::SplashScreen::SplashScreenBase::SplashScreenSettings settings;

        Local <Object> obj = Local<Object>::Cast($args[0]);

        GuiProxy::processValue<int>(obj, "x", [&](const int $value) -> void {
            settings.x = $value;
        });

        GuiProxy::processValue<int>(obj, "y", [&](const int $value) -> void {
            settings.y = $value;
        });

        GuiProxy::processValue<int>(obj, "width", [&](const int $value) -> void {
            settings.width = $value;
        });

        GuiProxy::processValue<int>(obj, "height", [&](const int $value) -> void {
            settings.height = $value;
        });

        GuiProxy::processValue<std::string>(obj, "imagePath", [&](const std::string &$value) -> void {
            settings.imagePath = $value;
        });

        try {
            GuiProxy::splashScreenInstance->setSettings(settings);
            GuiProxy::splashScreenInstance->Create();
        } catch (std::exception &ex) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "createSplashScreen ex").ToLocalChecked()));
        }
    }

    void GuiProxy::SplashScreenShow(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();

        if (!GuiProxy::splashScreenInstance) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "SplashScreen not initialized.").ToLocalChecked()));
            return;
        }

        GuiProxy::splashScreenInstance->Show();
    }

    void GuiProxy::SplashScreenHide(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();

        if (!GuiProxy::splashScreenInstance) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "SplashScreen not initialized.").ToLocalChecked()));
            return;
        }

        GuiProxy::splashScreenInstance->Hide();
    }

    void GuiProxy::SplashScreenSetOnOpenCB(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();

        if ($args.Length() != 1) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "Missing parameter.").ToLocalChecked()));
            return;
        }

        if (!$args[0]->IsFunction()) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "Parameter not function.").ToLocalChecked()));
            return;
        }

        GuiProxy::splashScreenOnOpenCallback.Reset(isolate, Local<v8::Function>::Cast($args[0]));

        const auto onOpenCallback = [](void) -> void {
            // todo(mkelnar) Fix callbacks, main issue is on WIN where v8 detects cross-thread access to isolate from callback call
            // auto cb = Local<v8::Function>::New(isolate, GuiProxy::splashScreenOnOpenCallback);
            // cb->Call(context, Null(isolate), 0, {}).ToLocalChecked();
        };

        if (!GuiProxy::splashScreenInstance) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "SplashScreen not created.").ToLocalChecked()));
            return;
        }

        GuiProxy::splashScreenInstance->setOnOpenCallback(onOpenCallback);
    }

    void GuiProxy::SplashScreenSetOnCloseCB(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();

        if ($args.Length() != 1) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "Missing parameter.").ToLocalChecked()));
            return;
        }

        if (!$args[0]->IsFunction()) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "Parameter not function.").ToLocalChecked()));
            return;
        }

        GuiProxy::splashScreenOnCloseCallback.Reset(isolate, Local<v8::Function>::Cast($args[0]));

        const auto onCloseCallback = [](void) -> void {
            // todo(mkelnar) Fix callbacks, main issue is on WIN where v8 detects cross-thread access to isolate from callback call
            // auto cb = Local<v8::Function>::New(isolate, GuiProxy::splashScreenOnCloseCallback);
            // cb->Call(context, Null(isolate), 0, {}).ToLocalChecked();
        };

        if (!GuiProxy::splashScreenInstance) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "SplashScreen not created.").ToLocalChecked()));
            return;
        }

        GuiProxy::splashScreenInstance->setOnCloseCallback(onCloseCallback);
    }

    void GuiProxy::SplashScreenAddProgressBar(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();
        Local <Context> context = isolate->GetCurrentContext();

        if ($args.Length() != 1) {
            isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(
                    isolate, "Missing argument.").ToLocalChecked()));
            return;
        }

        if (!$args[0]->IsObject()) {
            isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(
                    isolate, "Expected object argument.").ToLocalChecked()));
            return;
        }

        if (!GuiProxy::splashScreenInstance) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "SplashScreen not created.").ToLocalChecked()));
            return;
        }

        Gui::ProgressBar::ProgressBarBase::ProgressBarSettings settings;
        Local <Object> obj = Local<Object>::Cast($args[0]);

        GuiProxy::processValue<int>(obj, "x", [&](const int $value) -> void {
            settings.x = $value;
        });

        GuiProxy::processValue<int>(obj, "y", [&](const int $value) -> void {
            settings.y = $value;
        });

        GuiProxy::processValue<int>(obj, "width", [&](const int $value) -> void {
            settings.width = $value;
        });

        GuiProxy::processValue<int>(obj, "height", [&](const int $value) -> void {
            settings.height = $value;
        });

        GuiProxy::processValue<std::string>(obj, "foreground", [&](const std::string &$value) -> void {
            settings.foreground = $value;
        });

        GuiProxy::processValue<std::string>(obj, "background", [&](const std::string &$value) -> void {
            settings.background = $value;
        });

        settings.visible = true;
        settings.isMarquee = false;

        GuiProxy::splashScreenInstance->setProgressBar(settings);
    }

    void GuiProxy::SplashScreenSetProgressBarValue(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();

        if ($args.Length() != 1) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "Invalid parameters.").ToLocalChecked()));
            return;
        }

        if (!$args[0]->IsInt32()) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "Incorrect parameter type.").ToLocalChecked()));
            return;
        }

        if (!GuiProxy::splashScreenInstance) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "SplashScreen not created.").ToLocalChecked()));
            return;
        }

        GuiProxy::splashScreenInstance->setProgressBarValue($args[0].As<Integer>()->Value());
    }

    void GuiProxy::SplashScreenAddProgressLabel(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        GuiProxy::SplashScreenAddLabel(0, $args);
    }

    void GuiProxy::SplashScreenSetProgressLabelValue(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        GuiProxy::SplashScreenSetLabelValue(0, $args);
    }

    void GuiProxy::SplashScreenAddStatusLabel(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        GuiProxy::SplashScreenAddLabel(1, $args);
    }

    void GuiProxy::SplashScreenSetStatusLabelValue(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        GuiProxy::SplashScreenSetLabelValue(1, $args);
    }

    void GuiProxy::NotifyIconFactoryCreate(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();

        try {
            GuiProxy::notifyIconInstance.reset(Gui::NotifyIcon::NotifyIconFactory::Create());
        } catch (std::exception &ex) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "createSplashScreen ex").ToLocalChecked()));
        }
    }

    void GuiProxy::TaskBarFactoryCreate(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();

        try {
            GuiProxy::taskBarInstance.reset(Gui::TaskBar::TaskBarFactory::Create(nullptr));
        } catch (std::exception &ex) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "createTaskBar exception").ToLocalChecked()));
        }
    }

    void GuiProxy::TaskBarSetProgressValue(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();

        if (!GuiProxy::taskBarInstance) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "TaskBar instance not initialized.").ToLocalChecked()));
        }

        if ($args.Length() != 2) {
            isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(
                    isolate, "Invalid parameters.").ToLocalChecked()));
            return;
        }

        if (!$args[0]->IsInt32() || !$args[1]->IsInt32()) {
            isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(
                    isolate, "Incorrect parameter types.").ToLocalChecked()));
            return;
        }

        try {
            GuiProxy::taskBarInstance->setProgressValue($args[0].As<Integer>()->Value(),
                                                        $args[1].As<Integer>()->Value());
        } catch (std::exception &ex) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "TaskBar.SetProgressValue failed").ToLocalChecked()));
        }
    }

    void GuiProxy::TaskBarSetProgressState(const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();

        if (!GuiProxy::taskBarInstance) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "TaskBar instance not initialized.").ToLocalChecked()));
        }

        if ($args.Length() != 1) {
            isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(
                    isolate, "Invalid parameters.").ToLocalChecked()));
            return;
        }

        if (!$args[0]->IsInt32()) {
            isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(
                    isolate, "Incorrect parameter types.").ToLocalChecked()));
            return;
        }

        try {
            GuiProxy::taskBarInstance->setProgressState(
                    static_cast<Gui::TaskBar::TaskBarBase::TaskbarProgressState>($args[0].As<Integer>()->Value()));
        } catch (std::exception &ex) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "TaskBar.SetProgressState failed").ToLocalChecked()));
        }
    }

    void GuiProxy::SplashScreenAddLabel(int $id, const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();
        Local <Context> context = isolate->GetCurrentContext();

        if ($args.Length() != 1) {
            isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(
                    isolate, "Missing argument.").ToLocalChecked()));
            return;
        }

        if (!$args[0]->IsObject()) {
            isolate->ThrowException(v8::Exception::TypeError(String::NewFromUtf8(
                    isolate, "Expected object argument.").ToLocalChecked()));
            return;
        }

        if (!GuiProxy::splashScreenInstance) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "SplashScreen not created.").ToLocalChecked()));
            return;
        }

        Gui::Label::LabelBase::LabelSettings settings;
        Local <Object> obj = Local<Object>::Cast($args[0]);

        GuiProxy::processValue<int>(obj, "x", [&](const int $value) -> void {
            settings.x = $value;
        });

        GuiProxy::processValue<int>(obj, "y", [&](const int $value) -> void {
            settings.y = $value;
        });

        GuiProxy::processValue<int>(obj, "width", [&](const int $value) -> void {
            settings.width = $value;
        });

        GuiProxy::processValue<int>(obj, "height", [&](const int $value) -> void {
            settings.height = $value;
        });

        GuiProxy::processValue<std::string>(obj, "font", [&](const std::string &$value) -> void {
            settings.font = $value;
        });

        GuiProxy::processValue<std::string>(obj, "color", [&](const std::string &$value) -> void {
            settings.color = $value;
        });

        GuiProxy::processValue<int>(obj, "size", [&](const int $value) -> void {
            settings.size = $value;
        });

        GuiProxy::processValue<int>(obj, "justify", [&](const int $value) -> void {
            Gui::Label::LabelBase::TextJustify justify = Gui::Label::LabelBase::TextJustify::LEFT;

            switch ($value) {
                case 1:
                    justify = Gui::Label::LabelBase::TextJustify::CENTER;
                    break;
                case 2:
                    justify = Gui::Label::LabelBase::TextJustify::RIGHT;
                    break;
                default:
                    justify = Gui::Label::LabelBase::TextJustify::LEFT;
            }
            settings.justify = justify;
        });

        settings.visible = true;
        settings.bold = false;

        if ($id == 0) {
            GuiProxy::splashScreenInstance->setProgressLabel(settings);
        } else if ($id == 1) {
            GuiProxy::splashScreenInstance->setStatusLabel(settings);
        }
    }

    void GuiProxy::SplashScreenSetLabelValue(int $id, const v8::FunctionCallbackInfo <v8::Value> &$args) {
        Isolate *isolate = Isolate::GetCurrent();
        Local <Context> context = isolate->GetCurrentContext();

        if ($args.Length() != 1) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "Invalid parameters.").ToLocalChecked()));
            return;
        }

        if (!$args[0]->IsString()) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "Incorrect parameter type.").ToLocalChecked()));
            return;
        }

        if (!GuiProxy::splashScreenInstance) {
            isolate->ThrowException(v8::Exception::Error(String::NewFromUtf8(
                    isolate, "SplashScreen not created.").ToLocalChecked()));
            return;
        }
        String::Utf8Value imagePathUtf(isolate, $args[0]->ToString(context).ToLocalChecked());

        if ($id == 0) {
            GuiProxy::splashScreenInstance->setProgressLabelText(std::string(*imagePathUtf));
        } else if ($id == 1) {
            GuiProxy::splashScreenInstance->setStatusLabelText(std::string(*imagePathUtf));
        }
    }
}  // namespace node

NODE_MODULE_CONTEXT_AWARE_INTERNAL(gui,
        node::GuiProxy::Initialize
)

/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef SRC_GUI_H_
#define SRC_GUI_H_

#if defined(NODE_WANT_INTERNALS) && NODE_WANT_INTERNALS

#include <string>
#include <memory>
#include <functional>

#include "node_internals.h"

#include "onion_proxy.h"

namespace node {
    template<typename T> static T convertValue(const v8::Local<v8::Value> &$value);
    template<>
    int convertValue<int>(const v8::Local<v8::Value> &$value) {
        return $value.As<v8::Integer>()->Value();
    }

    template<>
    std::string convertValue<std::string>(const v8::Local<v8::Value> &$value) {
        v8::Isolate *isolate = v8::Isolate::GetCurrent();
        v8::Local <v8::Context> context = isolate->GetCurrentContext();
        v8::String::Utf8Value imagePathUtf(isolate, $value->ToString(context).ToLocalChecked());
        return std::string(*imagePathUtf);
    }

    class GuiProxy {
     public:
        static void Initialize(v8::Local <v8::Object> $target, v8::Local <v8::Value> $unused, v8::Local <v8::Context> $context);

        static void Dummy(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void SplashScreenUpdate(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void SplashScreenFactoryCreate(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void SplashScreenShow(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void SplashScreenHide(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void SplashScreenSetOnOpenCB(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void SplashScreenSetOnCloseCB(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void SplashScreenAddProgressBar(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void SplashScreenSetProgressBarValue(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void SplashScreenAddProgressLabel(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void SplashScreenSetProgressLabelValue(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void SplashScreenAddStatusLabel(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void SplashScreenSetStatusLabelValue(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void NotifyIconFactoryCreate(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void TaskBarFactoryCreate(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void TaskBarSetProgressValue(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void TaskBarSetProgressState(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void Finalize(const v8::FunctionCallbackInfo <v8::Value> &$args);

        static std::shared_ptr<Gui::SplashScreen::SplashScreenBase> splashScreenInstance;

     private:
        static std::shared_ptr<Gui::NotifyIcon::NotifyIconBase> notifyIconInstance;

        static std::shared_ptr<Gui::TaskBar::TaskBarBase> taskBarInstance;

        static v8::Persistent <v8::Function> splashScreenOnOpenCallback;

        static v8::Persistent <v8::Function> splashScreenOnCloseCallback;

        static void SplashScreenAddLabel(int $id, const v8::FunctionCallbackInfo <v8::Value> &$args);

        static void SplashScreenSetLabelValue(int $id, const v8::FunctionCallbackInfo <v8::Value> &$args);

        template<typename T>
        static void processValue(const v8::Local <v8::Object> &$object, const std::string &$property,
                                       const std::function<void(T)> &$handler) {
            v8::Isolate *isolate = v8::Isolate::GetCurrent();
            v8::Local <v8::Context> context = isolate->GetCurrentContext();
            auto key = v8::String::NewFromUtf8(isolate, $property.c_str()).ToLocalChecked();
            if ($object->HasOwnProperty(context, key).FromMaybe(false)) {
                auto value = $object->Get(context, key).ToLocalChecked();
                T valueConv;
                bool status = false;
                if (typeid(T) == typeid(int)) {
                    status = value->IsInt32();
                } else if (typeid(T) == typeid(std::string)) {
                    status = value->IsString();
                }

                if (!status) {
                    isolate->ThrowException(v8::Exception::TypeError(v8::String::NewFromUtf8(
                            isolate, (string("Invalid '") + $property + "' argument type").c_str()).ToLocalChecked()));
                    return;
                }

                if ($handler != nullptr) {
                    $handler(convertValue<T>(value));
                }
            }
        }
    };
}  // namespace node

#endif  // defined(NODE_WANT_INTERNALS) && NODE_WANT_INTERNALS

#endif  // SRC_GUI_H_

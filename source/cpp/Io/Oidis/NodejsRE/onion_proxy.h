/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef SRC_ONION_PROXY_H_
#define SRC_ONION_PROXY_H_

#include <ResourceManager/ResourceManager.hpp>
// #include <Gui/Gui.hpp>

namespace ResourceManager = Io::Oidis::Onion::ResourceManager;
// namespace Gui = Io::Oidis::Onion::Gui;

#endif  // SRC_ONION_PROXY_H_

/* ********************************************************************************************************* *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/**
 * This main observer is applied only in embedded application build.
 * To enable this file has to be added into other *.js sources in node.gyp (not needed for node 18 - globbing) configuration and
 * node.cc "StartExecution()" function has to been modified to run this file
 * (search for something like StartExecution(env, "internal/main/eval_string") to navigate into proper section)
 * Be sure that this execution override is applied only within embedded mode and no other loader is used before
 * (this could cause load by different algorithm with forced require("path").resolve or something else which
 * could corrupt internal "loader.min.js" passing as entrypoint - this wrong behaviour is done at least
 * by check_syntax and run_main_module)
 *
 * Externally defined ENVs - remember that these ENVs are global and thus will be passed over process.env to each child processes
 *   until env object will be manually cleared!
 * OIDIS_NODEJS_MAIN_SKIP_PATH_RESOLVE  - disable require("path").resolve(...) invocation over input script (could pass relative paths)
 * OIDIS_NODEJS_EMULATE_EMBEDDED  - switch node into embedded mode even when no embedded files exists
 * OIDIS_NODEJS_SKIP_LOADER  - loader.min.js will be skipped in embedded mode (this switch is ignored in common mode)
 * OIDIS_NODEJS_NODE_PATH  - overload path into node_modules folder
 * OIDIS_NODEJS_NO_RESOURCE_MANAGER  - disable resource_manager initialization
 *
 * node process could be called as usual if node behaviour is taken in account (use "node --help" for full info)
 *   - node [options] [script.js] [arguments]
 *   - node inspect [options] [script.js | host:port] [arguments]
 *   both examples have more less the same behaviour for resulting application. "[options]" are Node.js options listed i.e. by "node --help"
 *   and are not passed into process.argv in JavaScript (same as inspect command). So if process.argv[0] contains *.js file than
 *   internal loader.min.js script could be skipped and node will be used as regular one instead as embedded application
 */

'use strict';

const {RegExpPrototypeExec} = primordials;

const {
    prepareMainThreadExecution,
    markBootstrapComplete
} = require('internal/process/pre_execution');

prepareMainThreadExecution(false);  // oidis: do not expand argv1... we are in embedded loader

markBootstrapComplete();

const debug = require("util").debuglog("main");

function exitHandler($loader, $exitCode) {
    if ($loader) {
        $loader.destroy();
    }
    // automatically exits with the given exit code...
}

const path = require('path');
debug("Input process args: " + JSON.stringify(process.argv));
let main = "";
if (process.argv.length > 1) {
    main = process.argv[1];

    if (!process.env.hasOwnProperty("OIDIS_NODEJS_MAIN_SKIP_PATH_RESOLVE")) {
        // todo(mkelnar) Expand process.argv[1] into a full path if needed, this could be bypassed
        main = path.resolve(process.argv[1]);
    }
}

if (process.argv.length === 1 || (process.argv[1] && !main.match(/.*\.(js|cjs|mjs)$/g))) {
    if (process.env.hasOwnProperty("NODEJSRE_NODE_PATH")) {
        process.env.NODE_PATH = process.env.NODEJSRE_NODE_PATH;
    }
    if (process.env.hasOwnProperty("OIDIS_NODEJS_NODE_PATH")) {
        process.env.NODE_PATH = process.env.OIDIS_NODEJS_NODE_PATH;
    }
    // todo(mkelnar) move resource manager init to first require/module-import of native fs?
    if (!process.env.hasOwnProperty("OIDIS_NODEJS_NO_RESOURCE_MANAGER")) {
        const resourceManager = require("resource_manager");
        resourceManager.init();
        // "normal" exit for CTRL+C, SIGTERM and uncaught exception
        process.on('exit', exitHandler.bind(null, resourceManager));
    }
    if (!process.env.hasOwnProperty("NODEJSRE_SKIP_LOADER") && !process.env.hasOwnProperty("OIDIS_NODEJS_SKIP_LOADER")) {
        main = "/snapshot/resource/javascript/loader.min.js";
        if (!globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty(main)) {
            debug("resource manager loader override: cjs")
            main = "/snapshot/resource/javascript/loader.min.cjs";
        }
        if (!globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty(main)) {
            debug("resource manager loader override: mjs")
            main = "/snapshot/resource/javascript/loader.min.mjs";
        }

        // insert new loader at index=1 for possible handling in JS even when it is loaded internally below
        process.argv.splice(1, 0, main);
    }
}
debug("Process args after loader resolved: " + JSON.stringify(process.argv));

function handleProcessExit() {
    process.exitCode ??= 13;
}

async function handleMainPromise(promise) {
    process.on('exit', handleProcessExit);
    try {
        return await promise;
    } finally {
        process.off('exit', handleProcessExit);
    }
}

const {
    ObjectCreate,
    StringPrototypeEndsWith,
} = primordials;

const CJSLoader = require('internal/modules/cjs/loader');
const {Module, readPackageScope} = CJSLoader;

function runMainESM(mainPath) {
    const {loadESM} = require('internal/process/esm_loader');
    const {pathToFileURL} = require('internal/url');

    handleMainPromise(loadESM((esmLoader) => {
        const main = path.isAbsolute(mainPath) ?
            pathToFileURL(mainPath).href : mainPath;
        debug("path is absolute: " + path.isAbsolute(mainPath) + "; main: " + main);
        return esmLoader.import(main, undefined, ObjectCreate(null));
    }));
}

function shouldUseESMLoader(mainPath) {
    // Determine the module format of the main
    if (mainPath && StringPrototypeEndsWith(mainPath, '.mjs'))
        return true;
    if (!mainPath || StringPrototypeEndsWith(mainPath, '.cjs'))
        return false;
    const pkg = readPackageScope(mainPath);
    return pkg && pkg.data.type === 'module';
}

if (main === "") {
    const err = new Error("Input script path missing.");
    err.code = 'SCRIPT_NOT_FOUND';
    throw err;
} else {
    // require('internal/modules/cjs/loader').Module.runMain(main);
    // Module._load(main, null, true);
    const useESMLoader = shouldUseESMLoader(main);
    debug("should use ESM: " + useESMLoader + " (for: " + main + ")");
    if (useESMLoader) {
        runMainESM(main);
    } else {
        // Module._load is the monkey-patchable CJS module loader.
        Module._load(main, null, true);
    }
}

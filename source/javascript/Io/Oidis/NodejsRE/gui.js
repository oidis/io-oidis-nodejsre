/* ********************************************************************************************************* *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

// todo(mkelnar) UI support is temporarily disabled for all platforms because it is not stable and not finished
//  potential use is only on WIN platform but we have selfextractor for that so code is only commented out until final decision

// const gui = process.binding("gui");
//
// module.exports = {
//     dummy: gui.dummy,
//     SplashScreenFactoryCreate: gui.SplashScreenFactoryCreate,
//     SplashScreenShow: gui.SplashScreenShow,
//     SplashScreenHide: gui.SplashScreenHide,
//     SplashScreenSetOnOpenCB: gui.SplashScreenSetOnOpenCB,
//     SplashScreenSetOnCloseCB: gui.SplashScreenSetOnCloseCB,
//     SplashScreenAddProgressBar: gui.SplashScreenAddProgressBar,
//     SplashScreenUpdate: gui.SplashScreenUpdate,
//     SplashScreenSetProgressBarValue: gui.SplashScreenSetProgressBarValue,
//     SplashScreenAddProgressLabel: gui.SplashScreenAddProgressLabel,
//     SplashScreenSetProgressLabelValue: gui.SplashScreenSetProgressLabelValue,
//     SplashScreenAddStatusLabel: gui.SplashScreenAddStatusLabel,
//     SplashScreenSetStatusLabelValue: gui.SplashScreenSetStatusLabelValue,
//     NotifyIconFactoryCreate: gui.NotifyIconFactoryCreate,
//     TaskBarFactoryCreate: gui.TaskBarFactoryCreate,
//     TaskBarSetProgressValue: gui.TaskBarSetProgressValue,
//     TaskBarSetProgressState: gui.TaskBarSetProgressState,
//     Finalize: gui.Finalize
// };

/* ********************************************************************************************************* *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

const fs = require("fs");
const path = require("path");
const nativeModule = require("module");
const util = require("util");
const debug = util.debuglog("resource_manager");

let snapshotPath = "/snapshot";

if (!globalThis.hasOwnProperty("oidisStuffInitialized")) {
    debug("global this initialized");
    globalThis.oidisStuffInitialized = true;
}

function logFromLoader($message, $verbosity) {
    debug($verbosity + ", " + $message);
}

function normalizePath($path) {
    $path = $path.replace(/\\/g, "/").replace(/\/\//g, "/");
    if (!globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty($path)) {
        if ($path.startsWith("/resource/")) {
            if (globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty(snapshotPath + $path)) {
                $path = snapshotPath + $path;
            } else {
                $path = cwd + $path;
            }
        }
        if ($path.indexOf(":" + snapshotPath) > 0) {
            $path = $path.substring(2);
        }
    }
    return $path;
}

function getStats($path) {
    debug("stats: " + $path);
    let record = {};
    if (globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty($path) &&
        globalThis.oidisResourceManagerModuleSingleton.resourcesMap[$path].stats !== null) {
        record = JSON.parse(globalThis.oidisResourceManagerModuleSingleton.resourcesMap[$path].stats);
    }
    debug("stats-gt: " + globalThis.oidisResourceManagerModuleSingleton.hasOwnProperty("resourcesMap") + "; " + JSON.stringify(record));

    const now = new Date();
    return {
        isFile: function () {
            if (record.hasOwnProperty("isFile")) {
                return record.isFile;
            }
            return true;
        },
        isDirectory: function () {
            if (record.hasOwnProperty("isDirectory")) {
                return record.isDirectory;
            }
            return false;
        },
        isBlockDevice: function () {
            if (record.hasOwnProperty("isBlockDevice")) {
                return record.isBlockDevice;
            }
            return false;
        },
        isCharacterDevice: function () {
            if (record.hasOwnProperty("isCharacterDevice")) {
                return record.isCharacterDevice;
            }
            return false;
        },
        isSymbolicLink: function () {
            if (record.hasOwnProperty("isSymbolicLink")) {
                return record.isSymbolicLink;
            }
            return false;
        },
        isFIFO: function () {
            if (record.hasOwnProperty("isFIFO")) {
                return record.isFIFO;
            }
            return false;
        },
        isSocket: function () {
            if (record.hasOwnProperty("isSocket")) {
                return record.isSocket;
            }
            return false;
        },
        dev: 0,
        mode: record.hasOwnProperty("mode") ? record.mode : 0o666,
        nlink: 1,
        uid: 0,
        gid: 0,
        rdev: 0,
        ino: 0,
        size: 0,
        atimeMs: 0,
        mtimeMs: 0,
        ctimeMs: 0,
        birthtimeMs: 0,
        atime: record.hasOwnProperty("atime") ? new Date(record.atime) : now,
        mtime: record.hasOwnProperty("mtime") ? new Date(record.mtime) : now,
        ctime: record.hasOwnProperty("ctime") ? new Date(record.ctime) : now,
        birthtime: record.hasOwnProperty("birthtime") ? new Date(record.birthtime) : now,
    };
}

function isEmbeddedFile($path) {
    if (globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty($path)) {
        return getStats($path).isFile();
    }
    return false;
}

if (globalThis.oidisResourceManagerModuleSingleton.loader == null) {
    globalThis.oidisResourceManagerModuleSingleton.loader = process.binding("resource_manager");
    globalThis.oidisResourceManagerModuleSingleton.loader.initialize(process.argv0, logFromLoader);
    debug("All resources are cached");
}

module.exports = {
    init: function ($resourcesMap, $snapshotPath) {
        const cwd = path.normalize(path.dirname(process.argv[0])).replace(/\\/g, "/");
        debug("cwd: " + cwd + ", argv: " + JSON.stringify(process.argv));

        debug("Initializing resource manager");
        let isEmbedded = true;

        if ($resourcesMap !== undefined) {
            debug("Force used resources map.");
            globalThis.oidisResourceManagerModuleSingleton.resourcesMap = $resourcesMap;
            if ($snapshotPath !== undefined) {
                snapshotPath = $snapshotPath;
            }
        } else {
            try {
                if (globalThis.oidisResourceManagerModuleSingleton.resourcesMap == null) {
                    globalThis.oidisResourceManagerModuleSingleton.resourcesMap = {};
                    const packageMapData = globalThis.oidisResourceManagerModuleSingleton.loader.read("CONFIG", "PACKAGE_MAP", false, logFromLoader);
                    if (packageMapData !== null) {
                        globalThis.oidisResourceManagerModuleSingleton.resourcesMap = JSON.parse(packageMapData.toString());
                    } else {
                        isEmbedded = false;
                    }
                }
            } catch (ex) {
                debug("Failed to load resources map. Continuing in process without embedded resources.", ex);
                isEmbedded = false;
            }
        }
        globalThis.oidisResourceManagerModuleSingleton.isEmbedded = isEmbedded;

        this.fsInit();
    },

    fsInit: function () {
        debug("FSinit called");
        // fs module override is taken in account only when resource_manager.init() has been called before and embedded resources map
        // exists in file (only there the isEmbedded flag should be set up).
        // However, it is stored in globalThis so sanity check is necessary!
        if (globalThis.oidisResourceManagerModuleSingleton.loader != null &&
            globalThis.oidisResourceManagerModuleSingleton.resourcesMap != null &&
            globalThis.oidisResourceManagerModuleSingleton.isEmbedded) {
            debug("FSinit processing");
            process.env.NODE_PATH = snapshotPath + "/node_modules";
            const existsSync = fs.existsSync;
            fs.existsSync = function ($path) {
                if ($path === snapshotPath + "/node_modules") {
                    return true;
                }
                $path = normalizePath($path);
                debug("exists: " + $path);
                if (globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty($path)) {
                    return true;
                }
                return existsSync($path);
            };

            const statSync = fs.statSync;
            fs.statSync = function ($path, $options) {
                $path = normalizePath($path);
                if (globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty($path)) {
                    return getStats($path);
                }
                return statSync($path, $options);
            };
            const lstatSync = fs.lstatSync;
            fs.lstatSync = function ($path, $options) {
                $path = normalizePath($path);
                if (globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty($path)) {
                    return getStats($path);
                }
                return lstatSync($path, $options);
            };

            let stringExtensions = [
                ".js", ".json", ".jsonp", ".cjs", ".mjs", ".ts", ".tsx", ".es6", "es", ".jsx", ".json5",
                ".txt", ".xml", ".conf", ".yml", ".tpl",
                ".java", ".e4xmi", ".MF",
                ".html", ".css", ".map",
                ".cmd", ".sh",
                ".cpp", ".hpp", ".c", ".h", ".cmake", ".cc",
                ".key", ".crt", ".pem"
            ];
            fs.setStringExtensions = function ($value) {
                try {
                    stringExtensions = stringExtensions.concat($value);
                } catch (ex) {
                    debug("Failed to set string extensions. Exception: " + ex.toString());
                }
            }

            const readFileSync = fs.readFileSync;
            fs.readFileSync = function ($path, $options) {
                debug("read: " + $path);
                $path = normalizePath($path);
                if (globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty($path)) {
                    try {
                        const buffer = globalThis.oidisResourceManagerModuleSingleton.loader.read("RESOURCES",
                            globalThis.oidisResourceManagerModuleSingleton.resourcesMap[$path].name, false, logFromLoader);
                        if ($options !== null) {
                            if ($options === undefined) {
                                $options = "utf8";
                            } else if (typeof $options === "object" && $options.hasOwnProperty("encoding")) {
                                $options = $options.encoding;
                            }
                        }
                        if (stringExtensions.indexOf(path.extname($path)) !== -1) {
                            return buffer.toString($options);
                        }
                        return buffer;
                    } catch (ex) {
                        debug("exception: " + ex.toString());
                        // fallback to native processing
                    }
                }
                return readFileSync($path, $options);
            };

            const readFile = fs.readFile;
            fs.readFile = function ($path, $options, $callback) {
                $path = normalizePath($path);
                debug("read async: " + $path);
                if (globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty($path)) {
                    try {
                        if ($callback === undefined) {
                            $callback = $options;
                            $options = undefined;
                        }
                        $callback(null, fs.readFileSync($path, $options));
                    } catch (ex) {
                        $callback(ex);
                    }
                } else {
                    readFile($path, $options, $callback);
                }
            };

            const readdirSync = fs.readdirSync;
            fs.readdirSync = function ($path, $options) {
                $path = normalizePath($path);
                debug("readdir: " + $path);
                if (globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty($path) && getStats($path).isDirectory()) {
                    const content = [];
                    let key;
                    for (key in globalThis.oidisResourceManagerModuleSingleton.resourcesMap) {
                        if (key !== $path && globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty(key) && key.indexOf($path) !== -1) {
                            key = key.replace($path + "/", "");
                            if (key.indexOf("/") === -1) {
                                content.push(key);
                            }
                        }
                    }
                    return content;
                }
                return readdirSync($path, $options);
            };

            const createReadStream = fs.createReadStream;
            fs.createReadStream = function ($path, $options) {
                $path = normalizePath($path);
                debug("stream: " + $path);
                if (globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty($path)) {
                    const Readable = require("stream").Readable;
                    const stream = new Readable();
                    stream._read = () => {
                        // mock _read
                    };
                    stream.push(fs.readFileSync($path));
                    stream.push(null);
                    return stream;
                }
                return createReadStream($path, $options);
            };

            const copyFileSync = fs.copyFileSync;
            fs.copyFileSync = function ($src, $dest, $flags) {
                $src = normalizePath($src);
                debug("copyfile: '" + $src + "' -> '" + $dest + "'");
                if (globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty($src)) {
                    fs.writeFileSync($dest, globalThis.oidisResourceManagerModuleSingleton.loader.read("RESOURCES", globalThis.oidisResourceManagerModuleSingleton.resourcesMap[$src].name, false, logFromLoader));
                } else {
                    copyFileSync($src, $dest, $flags);
                }
            };

            const realpathSync = fs.realpathSync;
            fs.realpathSync = function ($path, $options) {
                $path = normalizePath($path);
                debug("realpathSync: '" + $path + "'");
                if (globalThis.oidisResourceManagerModuleSingleton.resourcesMap.hasOwnProperty($path)) {
                    return $path;
                } else {
                    realpathSync($path, $options);
                }
            }

            const _findPath = nativeModule._findPath;
            nativeModule._findPath = function ($request, $paths, $isMain) {
                let fileName = _findPath($request, $paths, $isMain);
                debug("findPath list: " + JSON.stringify($paths));
                const extension = Object.keys(nativeModule._extensions);
                const tryExtensions = function ($path) {
                    for (let index = 0; index < extension.length; index++) {
                        let path = $path + extension[index];
                        debug("findPath by ext: " + path);
                        if (isEmbeddedFile(path)) {
                            $path = path;
                            fileName = path;
                            break;
                        }
                    }
                    return $path;
                };
                if (!fileName) {
                    for (let index = 0; index < $paths.length; index++) {
                        let curPath = normalizePath(path.resolve($paths[index], $request));
                        debug("findPath: " + curPath);
                        if (isEmbeddedFile(curPath)) {
                            fileName = curPath;
                        } else {
                            curPath = tryExtensions(curPath);
                            if (!fileName && curPath.indexOf(".node") !== -1 && curPath.indexOf("/resource/libs") !== -1) {
                                fileName = process.nodejsRoot + curPath.substr(curPath.indexOf("/resource/libs"));
                            }
                        }
                        if (!fileName) {
                            const jsonPath = curPath + "/package.json";
                            debug("findPackage: " + jsonPath);
                            if (isEmbeddedFile(jsonPath)) {
                                const json = fs.readFileSync(jsonPath);
                                if (json !== undefined && json !== null) {
                                    try {
                                        const packageJson = JSON.parse(json);
                                        if (packageJson.hasOwnProperty("main")) {
                                            const mainScript = packageJson.main;
                                            if (mainScript !== undefined) {
                                                const main = normalizePath(path.resolve(curPath, mainScript));
                                                debug("findMain: " + mainScript + " [" + main + "]");
                                                if (isEmbeddedFile(main)) {
                                                    fileName = main;
                                                }
                                                if (!fileName) {
                                                    tryExtensions(main);
                                                }
                                                if (!fileName) {
                                                    tryExtensions(main + "/index");
                                                }
                                            }
                                        }
                                    } catch (ex) {
                                        ex.path = jsonPath;
                                        ex.message = "Error parsing " + jsonPath + ": " + ex.message;
                                        throw ex;
                                    }
                                }
                            }
                        } else {
                            break;
                        }
                        if (!fileName) {
                            tryExtensions(curPath + "/index");
                        } else {
                            break;
                        }
                    }
                    if (fileName !== false) {
                        const cacheKey = $request + "\x00" + ($paths.length === 1 ? $paths[0] : $paths.join("\x00"));
                        nativeModule._pathCache[cacheKey] = fileName;
                    }
                }
                return fileName;
            };

            const _resolveFilename = nativeModule._resolveFilename;
            nativeModule._resolveFilename = function ($request, $parent, $isMain, $options) {
                let filename;
                try {
                    filename = _resolveFilename($request, $parent, $isMain, $options);
                } catch (ex) {
                    if (ex.code !== "MODULE_NOT_FOUND") {
                        throw ex;
                    }
                    let isNative = false;
                    try {
                        isNative = require.resolve.paths($request) === null;
                    } catch (ex) {
                        debug("fail to identify module: " + $request);
                        isNative = false;
                    }
                    if (!isNative) {
                        if (!$request.startsWith(snapshotPath)) {
                            if ($request.startsWith("./") && $parent && $parent.filename) {
                                $request = normalizePath(path.resolve(path.dirname($parent.filename), $request));
                            } else {
                                $request = normalizePath(path.resolve(snapshotPath + "/node_modules/", $request));
                            }
                        }
                        debug("module: " + $request);
                    }
                    const savePathCache = nativeModule._pathCache;
                    nativeModule._pathCache = Object.create(null);
                    try {
                        filename = _resolveFilename($request, $parent, $isMain, $options);
                    } finally {
                        nativeModule._pathCache = savePathCache;
                    }
                }
                return filename;
            };

            // todo(mkelnar) there may be entered hack for embedded mjs modules load
            //  (es module alternative for _findPath/_resolveFilename in cjs space)
            //  ->es uses load.js and internal/fs/promises to async read file (over open FileHandle)... this may need to be completely
            //    replaced to allow load from embedded mjs source
        }
    },

    initialize: function ($path, $logCallback) {
        debug("Initialize resource_manager for file: " + $path);
        if ($path === undefined || $path === null) {
            throw new SyntaxError("Path parameter has to be defined and not null.");
        }
        globalThis.oidisResourceManagerModuleSingleton.loader.initialize($path, $logCallback);
    },

    read: function ($group, $id, $force, $logCallback) {
        debug("Read resource " + $group + "/" + $id);
        let isForce = false;
        let logCallback = null;
        if ($group === undefined || $group === null || $id === undefined || $id === null) {
            throw new TypeError("Group and ID parameters have to be defined and not null");
        }
        if (typeof $force === "boolean") {
            isForce = $force;
        }
        if (typeof $force === "function") {
            logCallback = $force;
        }
        if (typeof $logCallback === "function") {
            logCallback = $logCallback;
        }
        return globalThis.oidisResourceManagerModuleSingleton.loader.read($group, $id, isForce, logCallback);
    },

    write: function ($group, $id, $path, $logCallback) {
        debug("Write resource " + $group + "/" + $id);
        if ($group === undefined || $group === null || $id === undefined || $id === null || $path === undefined || $path === null) {
            throw new TypeError("Group, ID and path to resource parameters have to be defined and not null");
        }
        globalThis.oidisResourceManagerModuleSingleton.loader.write($group, $id, $path, $logCallback);
    },

    destroy: function () {
        debug("Destroying cached resources");
        globalThis.oidisResourceManagerModuleSingleton.loader.destroy();
    }
};

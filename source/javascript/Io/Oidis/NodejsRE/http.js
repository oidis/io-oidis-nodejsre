// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

'use strict';

const {
    ArrayPrototypeSlice,
    ArrayPrototypeSort,
    ObjectDefineProperty,
} = primordials;

const {validateInteger} = require('internal/validators');
const httpAgent = require('_http_agent');
const {ClientRequest} = require('_http_client');
const {methods, parsers} = require('_http_common');
const {IncomingMessage} = require('_http_incoming');
const {
    validateHeaderName,
    validateHeaderValue,
    OutgoingMessage
} = require('_http_outgoing');
const {
    _connectionListener,
    STATUS_CODES,
    Server,
    ServerResponse
} = require('_http_server');
let maxHeaderSize;

/**
 * Returns a new instance of `http.Server`.
 * @param {{
 *   IncomingMessage?: IncomingMessage;
 *   ServerResponse?: ServerResponse;
 *   insecureHTTPParser?: boolean;
 *   maxHeaderSize?: number;
 *   }} [opts]
 * @param {Function} [requestListener]
 * @returns {Server}
 */
function createServer(opts, requestListener) {
    return new Server(opts, requestListener);
}

/**
 * @typedef {object} HTTPRequestOptions
 * @property {httpAgent.Agent | boolean} [agent]
 * @property {string} [auth]
 * @property {Function} [createConnection]
 * @property {number} [defaultPort]
 * @property {number} [family]
 * @property {object} [headers]
 * @property {number} [hints]
 * @property {string} [host]
 * @property {string} [hostname]
 * @property {boolean} [insecureHTTPParser]
 * @property {string} [localAddress]
 * @property {number} [localPort]
 * @property {Function} [lookup]
 * @property {number} [maxHeaderSize]
 * @property {string} [method]
 * @property {string} [path]
 * @property {number} [port]
 * @property {string} [protocol]
 * @property {boolean} [setHost]
 * @property {string} [socketPath]
 * @property {number} [timeout]
 * @property {AbortSignal} [signal]
 */

/**
 * Makes an HTTP request.
 * @param {string | URL} url
 * @param {HTTPRequestOptions} [options]
 * @param {Function} [cb]
 * @returns {ClientRequest}
 */
function request(url, options, cb) {
    // oidis: begin-patch
    let switchToHttps = false;
    const checkPatch = function ($url) {
        try {
            const patchMap = JSON.parse(process.env.NODEJSRE_MOCK_SERVER_MAP);

            for (let patchMapKey in patchMap) {
                if (patchMap.hasOwnProperty(patchMapKey)) {
                    if (patchMapKey === $url) {
                        $url = patchMap[patchMapKey];
                        switchToHttps = $url.indexOf("https:") === 0;
                        break;
                    }
                }
            }
        } catch (e) {
            // failed, just continue without patch
        }

        return $url;
    };

    if (process.env.hasOwnProperty("NODEJSRE_MOCK_SERVER_MAP") && process.env.NODEJSRE_MOCK_SERVER_MAP.length > 0) {
        if (process.mainModule.filename.indexOf("/snapshot/") !== 0) {
            if (typeof url === 'string') {
                url = checkPatch(url);
            } else if (typeof url === 'object' && url.hasOwnProperty('href')) {
                let newHref = checkPatch(url.href);
                if (newHref !== url.href) {
                    url = newHref;
                }
            }
        } else {
            // "NODEJSRE_MOCK_SERVER_MAP found in environment but this is not supported in embedded mode";
        }
    }

    if (!switchToHttps) {
        return new ClientRequest(url, options, cb);
    } else {
        return require("https").request(url, options, cb);
    }
    // oidis: end-patch
}

/**
 * Makes a `GET` HTTP request.
 * @param {string | URL} url
 * @param {HTTPRequestOptions} [options]
 * @param {Function} [cb]
 * @returns {ClientRequest}
 */
function get(url, options, cb) {
    const req = request(url, options, cb);
    req.end();
    return req;
}

module.exports = {
    _connectionListener,
    METHODS: ArrayPrototypeSort(ArrayPrototypeSlice(methods)),
    STATUS_CODES,
    Agent: httpAgent.Agent,
    ClientRequest,
    IncomingMessage,
    OutgoingMessage,
    Server,
    ServerResponse,
    createServer,
    validateHeaderName,
    validateHeaderValue,
    get,
    request,
    setMaxIdleHTTPParsers(max) {
        validateInteger(max, 'max', 1);
        parsers.max = max;
    }
};

ObjectDefineProperty(module.exports, 'maxHeaderSize', {
    __proto__: null,
    configurable: true,
    enumerable: true,
    get() {
        if (maxHeaderSize === undefined) {
            const {getOptionValue} = require('internal/options');
            maxHeaderSize = getOptionValue('--max-http-header-size');
        }

        return maxHeaderSize;
    }
});

ObjectDefineProperty(module.exports, 'globalAgent', {
    __proto__: null,
    configurable: true,
    enumerable: true,
    get() {
        return httpAgent.globalAgent;
    },
    set(value) {
        httpAgent.globalAgent = value;
    }
});

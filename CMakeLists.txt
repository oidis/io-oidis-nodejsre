# * ********************************************************************************************************* *
# *
# * Copyright 2021 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

cmake_minimum_required(VERSION 3.14)

project(io-oidis-nodejsre)

set(CMAKE_CXX_STANDARD 14)

# THIS IS DUMMY CMAKE CONFIGURATION FILE. THE IO-OIDIS-NODEJSRE PROJECT USES NODE.GYP FOR PROJECT GENERATION AND BUILD. AND THIS CMAKE
# CONFIGURATION HAS BEEN ADDED HERE TO FIX CLION PROJECT LOAD ISSUES CONNECTED WITH MISSING CmakeLists.txt FILE (CAUSES ISSUES WITH
# MARKDOWN AND JAVASCRIPT CODE LINTERS AND OTHER STRANGE BEHAVIOURS

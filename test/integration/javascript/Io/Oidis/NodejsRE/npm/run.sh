#!/usr/bin/env bash

#export PATH=/var/oidis/io-oidis-nodejsre/build/releases/app-linux-gyp/target:$PATH
#export PATH=/tmp/webix/io-vit-application/dependencies/nodejs:$PATH
export PATH=/Users/nxa33894/workspace/oidis/io-oidis-nodejsre/build/releases/app-mac-gyp/target:$PATH
export NODEJSRE_MOCK_SERVER_MAP='{"https://nodejs.org/download/release/v14.15.3/win-x64/node.lib":"http://127.0.0.1:44193/node.lib","https://nodejs.org/dist/v14.15.3/win-x64/node.lib":"http://127.0.0.1:44193/node.lib","https://nodejs.org/download/release/v14.15.3/node-v14.15.3-headers.tar.gz":"http://127.0.0.1:44193/node-v14.15.3-headers.tar.gz","https://nodejs.org/dist/v14.15.3/node-v14.15.3-headers.tar.gz":"http://127.0.0.1:44193/node-v14.15.3-headers.tar.gz","https://nodejs.org/download/release/v14.15.3/SHASUMS256.txt":"http://127.0.0.1:44193/SHASUMS256.txt","https://nodejs.org/dist/v14.15.3/SHASUMS256.txt":"http://127.0.0.1:44193/SHASUMS256.txt"}'
export HTTP_PROXY="http://emea.nics.nxp.com:8080"
export HTTPS_PROXY="http://emea.nics.nxp.com:8080"

npm config set prefer-offline true
npm config set fetch-retries 25
npm config set fetch-retry-mintimeout 500
npm config set fetch-retry-maxtimeout 1000

rm -rf node_modules

npm install --verbose

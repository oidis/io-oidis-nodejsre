/* ********************************************************************************************************* *
 *
 * Copyright 2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

const assert = require("assert").strict;
const suites = [];
let filter = [];
let reportOnly = false;
if (process.argv.length > 2) {
    process.argv.forEach((attr) => {
        if (attr.indexOf("--filter=") === 0) {
            filter = attr.replace("--filter=", "").split(";")
        }
        if (attr.indexOf("--report-only") === 0) {
            reportOnly = true;
        }
    })
}

function matchFilter($key) {
    if (filter) {
        if (filter.length >= 1) {
            if (filter.findIndex($value => (new RegExp("^" + $value.replace("*", ".*"))).test($key)) !== -1) {
                return true;
            }
        }
    }
    return false;
}

async function runTest($suite, $testFunction) {
    const fontMark = "\x1b[33m%s\x1b[0m";

    if (matchFilter($testFunction.name)) {
        console.log(fontMark, " - Skipped test '" + $testFunction.name + "'\n");
        return 2;
    }

    await $suite.setUp();
    console.log(fontMark, " - Running test '" + $testFunction.name + "'");

    let status = 0;
    try {
        await $testFunction.method();
        status = 1;
    } catch ($e) {
        console.warn($e);
    }

    console.log(fontMark, " - Ending test '" + $testFunction.name + "'\n");
    await $suite.tearDown();

    return status;
}

async function runTestSuite($suite) {
    const fontMark = "\x1b[34m%s\x1b[0m";

    console.log(fontMark, "Running test suite '" + $suite.name + "'\n");

    let status = true;
    let skipped = 0;
    let info = [];
    for await(let item of $suite.tests) {
        const testStatus = await runTest($suite, item);
        if (testStatus === 0) {
            status = false;
        } else if (testStatus === 2) {
            skipped++;
        }
        info.push({
            status: testStatus,
            skipped: testStatus === 2,
            suite: $suite.name,
            method: item.name
        })
    }

    console.log(fontMark, "Ending test suite '" + $suite.name + "'\n");
    return {status, info, skipped};
}

function describe($name, $method) {
    suites[suites.length - 1].tests.push({name: $name, method: $method});
}

function ignore_describe($name, $method) {
    // dummy placeholder for ignored test
}

function setUp($method) {
    suites[suites.length - 1].setUp = $method;
}

function tearDown($method) {
    suites[suites.length - 1].tearDown = $method;
}

function suite($name, $handler) {
    suites.push({
        name: $name,
        tests: [],
        setUp: async () => {
            // dummy
        },
        tearDown: async () => {
            // dummy
        }
    });
    $handler();
}

async function runAllTests() {
    let info = [];
    let status = true;
    let skipped = 0;
    for await(const suite of suites) {
        const block = await runTestSuite(suite);
        if (!block.skipped) {
            status = status && block.status;
        }
        info = info.concat(block.info);
    }
    return {status, info, skipped};
}

function runTests() {
    runAllTests()
        .then(($summary) => {
            console.log("\x1b[35m%s\x1b[0m", "TESTS EXECUTION SUMMARY");
            let failedTests = 0;
            let skippedTests = 0;
            let succeedTests = 0;
            for (const item of $summary.info) {
                let line = item.suite + " - " + item.method;
                let color;
                if (item.skipped) {
                    skippedTests++;
                    line = " ~ " + line;
                    color = "\x1b[33m%s\x1b[0m";
                } else {
                    if (item.status) {
                        succeedTests++;
                        line = " + " + line;
                        color = "\x1b[32m%s\x1b[0m";
                    } else {
                        failedTests++;
                        line = " - " + line;
                        color = "\x1b[31m%s\x1b[0m";
                    }
                }
                console.log(color, line);
            }

            console.log("\x1b[35m%s\x1b[0m", "TESTS SUCCEED: " + succeedTests);
            console.log("\x1b[35m%s\x1b[0m", "TESTS FAILED : " + failedTests);
            console.log("\x1b[35m%s\x1b[0m", "TESTS SKIPPED: " + skippedTests);
            if ($summary.status || reportOnly) {
                process.exit(0);
            } else {
                process.exit(-1);
            }
        })
        .catch(($e) => {
            console.error($e);
            process.exit(-1);
        })
}

function ignore_describe($name, $method) {
    // dummy placeholder for ignored test
}

module.exports = {
    assert,
    ignore_describe,
    describe,
    setUp,
    tearDown,
    suite,
    runTests
}

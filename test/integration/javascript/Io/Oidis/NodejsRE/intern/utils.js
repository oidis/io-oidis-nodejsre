/* ********************************************************************************************************* *
 *
 * Copyright 2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

const path = require("path");
const fs = require("fs");

async function execute($cmd, $args, $options) {
    let stdoutData = "";
    let stderrData = "";
    let child;
    const run = async () => {
        const spawn = require("child_process").spawn;
        return new Promise(($resolve, $reject) => {
            setTimeout(() => {
                if (!$options) {
                    $options = {};
                }
                $options.shell = false;
                $options.whindowsHide = true;
                $options.stdio = ["inherit", "pipe", "pipe"];  // allow stdin for default nodejs main;
                child = spawn($cmd, $args, $options);
                child.stdout.on("data", ($data) => {
                    stdoutData += $data;
                });
                child.stderr.on("data", ($data) => {
                    stderrData += $data;
                });
                child.on("exit", $resolve);
                child.on("close", $resolve);
                child.on("error", $reject);
            }, 100);
        })
    };
    await Promise.race([
        run(),
        new Promise(($resolve) => {
            setTimeout(() => {
                // wait for load
                $resolve();
            }, 2000);
        })
    ])
    try {
        child.kill();
    } catch (e) {
        console.warn("Unable to clean processes: " + e.message);
    }
    return {stdout: stdoutData, stderr: stderrData};
}

function normalize($path) {
    if (Array.isArray($path)) {
        return $path.map(item => normalize(item));
    }
    return $path.replace(/[\/\\]+/g, "/");
}

function copyRecursiveSync($src, $dest) {
    const exists = fs.existsSync($src);
    const stats = exists && fs.statSync($src);
    const isDirectory = exists && stats.isDirectory();
    if (isDirectory) {
        if (!fs.existsSync($dest)) {
            fs.mkdirSync($dest, {recursive: true});
        }
        fs.readdirSync($src).forEach(function (childItemName) {
            copyRecursiveSync(path.join($src, childItemName),
                path.join($dest, childItemName));
        });
    } else {
        fs.copyFileSync($src, $dest);
    }
}

function lineSplit($data) {
    return $data.split(/[\r\n]+/g).filter(($value) => {
        return $value.length > 0
    });
}

module.exports = {
    execute,
    normalize,
    copyRecursiveSync,
    lineSplit
}

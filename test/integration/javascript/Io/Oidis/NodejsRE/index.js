/* ********************************************************************************************************* *
 *
 * Copyright 2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

const {runTests} = require("./intern/testoid");

require("./suites/node_http_patch_test");
require("./suites/node_cli_test");
require("./suites/ResourceManager/resource_manager_read_test");
require("./suites/ResourceManager/resource_manager_write_test");
require("./suites/ResourceManager/resource_manager_write_read_test");
// require("./suites/Gui/gui_test");

runTests();

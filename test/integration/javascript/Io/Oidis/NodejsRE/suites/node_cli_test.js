/* ********************************************************************************************************* *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

const {assert, describe, setUp, tearDown, suite, ignore_describe} = require("../intern/testoid");
const {execute, normalize, copyRecursiveSync, lineSplit} = require("../intern/utils");

const path = require("path");
const fs = require("fs");
const os = require("os");
const isWin = (os.platform().toString() === "win32" || os.platform().toString() === "win64");

const nodeBin = normalize(path.resolve(process.argv0));
const cwd = normalize(process.cwd());
const destFolder = cwd + "/build/target/test";
const profDir = cwd + "/prof-dir";

const generalTearDown = () => {
    [
        cwd + "/packedMap.json",
        cwd + "/test.cjs",
        cwd + "/test.mjs",
        cwd + "/test.js",
        cwd + "/testData",
        cwd + "/prof-dir",
        destFolder
    ].forEach(($path) => {
        if (fs.existsSync($path)) {
            fs.rmSync($path, {force: true, recursive: true, maxRetries: 10});
        }
    });
}

const createCJSFullContent = () => {
    fs.writeFileSync(cwd + "/test.js", "" +
        "if (process.argv.indexOf('--version') >= 0) {\n" +
        "    console.log('v666.666.666');\n" +
        "    process.exit(0);" +
        "}\n" +
        "if (process.argv.indexOf('--help') >= 0) {\n" +
        "    console.log('THIS IS HELP OVERRIDE');\n" +
        "    process.exit(0);" +
        "}\n" +
        "const data = Buffer.from(JSON.stringify({\n" +
        "    argv: process.argv,\n" +
        "    cwd: process.cwd(),\n" +
        "    env: process.env,\n" +
        "    dirname: __dirname,\n" +
        "    filename: __filename\n" +
        "})).toString('base64');\n" +
        "console.log('> run context: '+data);\n");
}

const createShortContent = ($isESM, $cwd, $withPkgJson) => {
    if ($cwd !== "" && $cwd !== undefined) {
        if (fs.existsSync($cwd)) {
            fs.rmSync($cwd, {force: true, recursive: true});
        }
        fs.mkdirSync($cwd);
    } else {
        $cwd = cwd;
    }
    if (!$isESM) {
        fs.writeFileSync($cwd + "/test.js", "" +
            "const data = Buffer.from(JSON.stringify({\n" +
            "    argv: process.argv,\n" +
            "    isCJS: (typeof module !== 'undefined')\n" +
            "})).toString('base64');\n" +
            "console.log('> run context: '+data);\n");
        fs.writeFileSync($cwd + "/test.cjs", "" +
            "if (process.argv.indexOf('--version') >= 0) {\n" +
            "    console.log('v666.666.666');\n" +
            "    process.exit(0);" +
            "}\n" +
            "const data = Buffer.from(JSON.stringify({\n" +
            "    argv: process.argv,\n" +
            "    isCJS: (typeof module !== 'undefined')\n" +
            "})).toString('base64');\n" +
            "console.log('> run context: '+data);\n");
        if ($withPkgJson) {
            fs.writeFileSync($cwd + "/package.json",
                JSON.stringify({
                    "type": "commonjs"
                }));
        }
    } else {
        fs.writeFileSync($cwd + "/test.js", "" +
            "import fs from 'fs';" +
            "const data = Buffer.from(JSON.stringify({\n" +
            "    argv: process.argv,\n" +
            "    isCJS: (typeof module !== 'undefined')\n" +
            "})).toString('base64');\n" +
            "console.log('> run context: '+data);\n");
        fs.writeFileSync($cwd + "/test.mjs", "" +
            "import fs from 'fs'\n" +
            "if (process.argv.indexOf('--version') >= 0) {\n" +
            "    console.log('v666.666.666');\n" +
            "    process.exit(0);" +
            "}\n" +
            "const data = Buffer.from(JSON.stringify({\n" +
            "    argv: process.argv,\n" +
            "    isCJS: (typeof module !== 'undefined')\n" +
            "})).toString('base64');\n" +
            "console.log('> run context: '+data);\n");
        if ($withPkgJson) {
            fs.writeFileSync($cwd + "/package.json",
                JSON.stringify({
                    "type": "module"
                }));
        }
    }
}

const createPackage = ($main) => {
    let ext = path.extname($main);
    const resourceManager = require("resource_manager");

    const sourceFolder = path.dirname(nodeBin);
    fs.mkdirSync(destFolder, {recursive: true});

    let destinationBinary = destFolder + "/node" + (isWin ? ".exe" : "");
    copyRecursiveSync(sourceFolder, destFolder);

    resourceManager.initialize(destinationBinary);
    const packageMap = {};
    packageMap["/snapshot/resource/javascript/loader.min" + ext] = {
        "name": "RES_PACKAGE_MAP",
        "stats": JSON.stringify(fs.statSync($main))
    };

    fs.writeFileSync(cwd + "/packedMap.json", JSON.stringify(packageMap));
    resourceManager.write("CONFIG", "PACKAGE_MAP", cwd + "/packedMap.json", function ($message, $verbosity) {
        //console.log($verbosity + ", " + $message);
    });
    resourceManager.write("RESOURCES", "RES_PACKAGE_MAP", $main, function ($message, $verbosity) {
        //console.log($verbosity + ", " + $message);
    });
    return destinationBinary;
}

suite(path.basename(__filename) + " - BASIC", () => {
    setUp(() => {
        createCJSFullContent();
    });

    tearDown(generalTearDown);

    describe("testNodeNoArg", async () => {
        if (process.stdin.isTTY) {
            const {stdout, stderr} = await execute(nodeBin);

            console.log(stdout);
            assert.equal(stderr, "");
            assert.equal([...stdout.matchAll(/^\s*Welcome to Node\.js\s*(v\d+\.\d+\.\d+)/g)][0][1], process.version);
        } else {
            console.warn("Ignored test due to missing TTY");
        }
    });

    describe("testNodeNoArgDeep", async () => {
        if (process.stdin.isTTY) {
            const {stdout, stderr} = await execute(nodeBin, [], {
                env: {
                    OIDIS_NODEJS_DEBUG: 1
                }
            });

            console.log(stdout);
            assert.equal(stderr, "");
            const lines = lineSplit(stdout);

            assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
            assert.equal(lines[1], "embedded: 0");
            assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[2])[1]), nodeBin);
            assert.equal(/Welcome to Node\.js\s*(v\d+\.\d+\.\d+)/g.exec(lines[3])[1], process.version);
        } else {
            console.warn("Ignored test due to missing TTY");
        }
    });

    describe("testNodeVersionArg", async () => {
        const {stdout, stderr} = await execute(nodeBin, ["--version"], {
            env: {
                OIDIS_NODEJS_DEBUG: 1
            }
        });

        assert.equal(stderr, "");
        const lines = lineSplit(stdout);

        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(lines[1], "item: --version");
        assert.equal(lines[2], "embedded: 0");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), nodeBin);
        assert.equal(lines[4], process.version);
    });

    describe("testNodeHelpArg", async () => {
        const {stdout, stderr} = await execute(nodeBin, ["--help"], {
            env: {
                OIDIS_NODEJS_DEBUG: 1
            }
        });

        assert.equal(stderr, "");
        const lines = lineSplit(stdout);
        // console.log(stdout);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(lines[1], "item: --help");
        assert.equal(lines[2], "embedded: 0");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), nodeBin);
        assert.equal(lines[4], "Usage: node [options] [ script.js ] [arguments]");
        assert.equal(lines[5].trim(), "node inspect [options] [ script.js | host:port ] [arguments]");
    });

    describe("testNodeAnyDashArg", async () => {
        const {stdout, stderr} = await execute(nodeBin, ["--path"], {
            env: {
                OIDIS_NODEJS_DEBUG: 1
            }
        });

        const lines = lineSplit(stdout);

        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(lines[1], "item: --path");
        assert.equal(lines[2], "embedded: 0");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), nodeBin);
        assert.equal(stderr.trim(), nodeBin + ": bad option: --path");
    });

    describe("testNodeDefaultRun", async () => {
        const testFile = cwd + "/test.js";
        const {stdout} = await execute(nodeBin, [testFile], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1
            }
        });

        const lines = lineSplit(stdout);

        // produced by internals
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), cwd + "/test.js");
        assert.equal(lines[2], "embedded: 0");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), nodeBin);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [nodeBin, testFile]);
        assert.equal(normalize(data.cwd), cwd);
        assert.equal(normalize(data.dirname), cwd);
        assert.equal(normalize(data.filename), testFile);
        if (data.env.hasOwnProperty("PWD")) {
            assert.equal(normalize(data.env.PWD), cwd);
        }
        assert.equal(data.env.OIDIS_NODEJS_DEBUG, "1");
        assert.ok(!data.env.OIDIS_NODEJS_MAIN_SKIP_PATH_RESOLVE);
        assert.ok(!data.env.OIDIS_NODEJS_EMULATE_EMBEDDED);
        assert.ok(!data.env.OIDIS_NODEJS_SKIP_LOADER);
        assert.ok(!data.env.OIDIS_NODEJS_NODE_PATH);
        assert.ok(!data.env.OIDIS_NODEJS_NO_RESOURCE_MANAGER);
        assert.ok(!data.env.OIDIS_NODEJS_EMBEDDED_MODE);
        assert.ok(!data.env.OIDIS_NODEJS_DEBUGGER_ATTACHED);
    });

    describe("testNodeEmbeddedRun", async () => {
        const {stdout, stderr} = await execute(nodeBin, [], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });

        const lines = lineSplit(stdout);

        // console.log(stdout);
        // console.warn(stderr);

        // produced by internals
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(lines[1], "embedded: 0");  // this is correct because this contains true only when some embedded files exists
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[2])[1]), nodeBin);

        if (!isWin) {
            assert.ok(stderr.match(/Cannot find module '\/snapshot\/resource\/javascript\/loader\.min\.mjs'/g), "ERROR: " + stderr);
        } else {
            assert.ok(stderr.match(/Cannot find module '\w:\\snapshot\\resource\\javascript\\loader\.min\.mjs'/gi), "ERROR: " + stderr);
        }
    });

    describe("testNodeEmbeddedInputScript", async () => {
        const testFile = cwd + "/test.js";
        const {stdout, stderr} = await execute(nodeBin,
            ["--cpu-prof", "--cpu-prof-dir=" + profDir, testFile, "SecondArg", "--ThirdArg"], {
                shell: true,
                windowsHide: true,
                env: {
                    OIDIS_NODEJS_DEBUG: 1,
                    OIDIS_NODEJS_EMULATE_EMBEDDED: 1
                }
            });

        const lines = lineSplit(stdout);

        // console.log(stdout);
        // console.warn(stderr);

        // produced by internals
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "--cpu-prof");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), "--cpu-prof-dir=" + profDir);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[3])[1]), testFile);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[4])[1]), "SecondArg");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[5])[1]), "--ThirdArg");
        assert.equal(lines[6], "embedded: 0");  // this is correct because this contains true only when some embedded files exists
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[7])[1]), nodeBin);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [nodeBin, testFile, "SecondArg", "--ThirdArg"]);
        assert.equal(normalize(data.cwd), cwd);
        assert.equal(normalize(data.dirname), cwd);
        assert.equal(normalize(data.filename), testFile);
        if (data.env.hasOwnProperty("PWD")) {
            assert.equal(normalize(data.env.PWD), cwd);
        }
        assert.equal(data.env.OIDIS_NODEJS_DEBUG, "1");
        assert.ok(!data.env.NODE_PATH);  // modules path should be empty if it is not set explicitly before node call
        assert.ok(!data.env.OIDIS_NODEJS_MAIN_SKIP_PATH_RESOLVE);
        assert.ok(data.env.OIDIS_NODEJS_EMULATE_EMBEDDED);
        assert.ok(!data.env.OIDIS_NODEJS_SKIP_LOADER);
        assert.ok(!data.env.OIDIS_NODEJS_NODE_PATH);
        assert.ok(!data.env.OIDIS_NODEJS_NO_RESOURCE_MANAGER);
        assert.ok(!data.env.OIDIS_NODEJS_DEBUGGER_ATTACHED);
    });

    describe("testNodeEmbeddedRunNoLoader", async () => {
        const testFile = cwd + "/test.js";
        const {stdout, stderr} = await execute(nodeBin,
            ["--cpu-prof", "--cpu-prof-dir=" + profDir, testFile, "SecondArg", "--ThirdArg"], {
                shell: true,
                windowsHide: true,
                env: {
                    OIDIS_NODEJS_DEBUG: 1,
                    OIDIS_NODEJS_EMULATE_EMBEDDED: 1,
                    OIDIS_NODEJS_SKIP_LOADER: 1
                }
            });

        const lines = lineSplit(stdout);

        // console.log(stdout);
        // console.warn(stderr);

        // produced by internals
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "--cpu-prof");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), "--cpu-prof-dir=" + profDir);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[3])[1]), cwd + "/test.js");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[4])[1]), "SecondArg");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[5])[1]), "--ThirdArg");
        assert.equal(lines[6], "embedded: 0");  // this is correct because this contains true only when some embedded files exists
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[7])[1]), nodeBin);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [nodeBin, testFile, "SecondArg", "--ThirdArg"]);
        assert.equal(normalize(data.cwd), cwd);
        assert.equal(normalize(data.dirname), cwd);
        assert.equal(normalize(data.filename), testFile);
        if (data.env.hasOwnProperty("PWD")) {
            assert.equal(normalize(data.env.PWD), cwd);
        }
        assert.equal(data.env.OIDIS_NODEJS_DEBUG, "1");
        assert.ok(!data.env.NODE_PATH);  // modules path should be empty if it is not set explicitly before node call
        assert.ok(!data.env.OIDIS_NODEJS_MAIN_SKIP_PATH_RESOLVE);
        assert.ok(data.env.OIDIS_NODEJS_EMULATE_EMBEDDED);
        assert.ok(data.env.OIDIS_NODEJS_SKIP_LOADER);
        assert.ok(!data.env.OIDIS_NODEJS_NODE_PATH);
        assert.ok(!data.env.OIDIS_NODEJS_NO_RESOURCE_MANAGER);
        assert.ok(!data.env.OIDIS_NODEJS_DEBUGGER_ATTACHED);
    });

    describe("testNodeEmbeddedRunNoLoaderNoScript", async () => {
        // embedded mode with skipped loader uses first argument as script
        // in this case any first argument will be passed into path resolver
        const {stdout, stderr} = await execute(nodeBin, ["FirstArgShouldBeScript", "--some"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1,
                OIDIS_NODEJS_SKIP_LOADER: 1
            }
        });

        const lines = lineSplit(stdout);

        // produced by internals
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "FirstArgShouldBeScript");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), "--some");
        assert.equal(lines[3], "embedded: 0");  // this is correct because this contains true only when some embedded files exists
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[4])[1]), nodeBin);

        assert.ok(normalize(stderr).match(new RegExp("Error: Cannot find module '" + cwd + "/FirstArgShouldBeScript'", 'g')));
    });

    describe("testNodeEmbeddedRunNoLoaderNoScriptNoResolve", async () => {
        // embedded mode with skipped loader uses first argument as script
        // in this case any first argument will be passed into path resolver
        const {stdout, stderr} = await execute(nodeBin, ["FirstArgShouldBeScript", "--some"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1,
                OIDIS_NODEJS_SKIP_LOADER: 1,
                OIDIS_NODEJS_MAIN_SKIP_PATH_RESOLVE: 1
            }
        });

        const lines = lineSplit(stdout);

        // produced by internals
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "FirstArgShouldBeScript");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), "--some");
        assert.equal(lines[3], "embedded: 0");  // this is correct because this contains true only when some embedded files exists
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[4])[1]), nodeBin);

        // has to be like this because path resolver is switched off!
        assert.ok(normalize(stderr).match(new RegExp("Error: Cannot find module '(\/.*)*FirstArgShouldBeScript'", 'g')), stderr);
    });

    describe("testNodeEmbeddedInspect", async () => {
        const testFile = cwd + "/test.js";
        const {stdout, stderr} = await execute(nodeBin, ["--inspect", testFile, "SecondArg", "--ThirdArg"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1,
                OIDIS_NODEJS_SKIP_LOADER: 1
            }
        });

        const lines = lineSplit(stdout);

        //console.log(stdout);
        //console.warn(stderr);

        // produced by internals
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "--inspect");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), cwd + "/test.js");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[3])[1]), "SecondArg");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[4])[1]), "--ThirdArg");
        assert.equal(lines[5], "embedded: 0");  // this is correct because this contains true only when some embedded files exists
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[6])[1]), nodeBin);

        // dont know why this is in stderr
        assert.ok(stderr.match(/Debugger listening on ws:/g));

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [nodeBin, testFile, "SecondArg", "--ThirdArg"]);
        assert.equal(normalize(data.cwd), cwd);
        assert.equal(normalize(data.dirname), cwd);
        assert.equal(normalize(data.filename), testFile);
        if (data.env.hasOwnProperty("PWD")) {
            assert.equal(normalize(data.env.PWD), cwd);
        }
        assert.equal(data.env.OIDIS_NODEJS_DEBUG, "1");
        assert.ok(!data.env.NODE_PATH);  // modules path should be empty if it is not set explicitly before node call
        assert.ok(!data.env.OIDIS_NODEJS_MAIN_SKIP_PATH_RESOLVE);
        assert.ok(data.env.OIDIS_NODEJS_EMULATE_EMBEDDED);
        assert.ok(data.env.OIDIS_NODEJS_SKIP_LOADER);
        assert.ok(!data.env.OIDIS_NODEJS_NODE_PATH);
        assert.ok(!data.env.OIDIS_NODEJS_NO_RESOURCE_MANAGER);
        assert.ok(data.env.OIDIS_NODEJS_DEBUGGER_ATTACHED);
    });

    describe("testNodeEmbeddedInspectNoFirst", async () => {
        const testFile = cwd + "/test.js";
        const {stdout, stderr} = await execute(nodeBin, [testFile, "SecondArg", "--ThirdArg", "--inspect"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1,
                OIDIS_NODEJS_SKIP_LOADER: 1
            }
        });

        const lines = lineSplit(stdout);

        // console.log(stdout);
        // console.warn(stderr);

        // produced by internals
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), cwd + "/test.js");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), "SecondArg");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[3])[1]), "--ThirdArg");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[4])[1]), "--inspect");
        assert.equal(lines[5], "embedded: 0");  // this is correct because this contains true only when some embedded files exists
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[6])[1]), nodeBin);

        // dont know why this is in stderr
        assert.ok(!stderr.match(/Debugger listening on ws:/g));

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [nodeBin, testFile, "SecondArg", "--ThirdArg", "--inspect"]);
        assert.equal(normalize(data.cwd), cwd);
        assert.equal(normalize(data.dirname), cwd);
        assert.equal(normalize(data.filename), testFile);
        if (data.env.hasOwnProperty("PWD")) {
            assert.equal(normalize(data.env.PWD), cwd);
        }
        assert.equal(data.env.OIDIS_NODEJS_DEBUG, "1");
        assert.ok(!data.env.NODE_PATH);  // modules path should be empty if it is not set explicitly before node call
        assert.ok(!data.env.OIDIS_NODEJS_MAIN_SKIP_PATH_RESOLVE);
        assert.ok(data.env.OIDIS_NODEJS_EMULATE_EMBEDDED);
        assert.ok(data.env.OIDIS_NODEJS_SKIP_LOADER);
        assert.ok(!data.env.OIDIS_NODEJS_NODE_PATH);
        assert.ok(!data.env.OIDIS_NODEJS_NO_RESOURCE_MANAGER);
        assert.ok(!data.env.OIDIS_NODEJS_DEBUGGER_ATTACHED);
    });
});

suite(path.basename(__filename) + " - resourceManager", () => {
    setUp(() => {
        createCJSFullContent()
    });

    tearDown(generalTearDown);

    describe("testLoader", async () => {
        const destinationBinary = createPackage(cwd + "/test.js");

        // run here?
        const {stdout, stderr} = await execute(destinationBinary, ["FirstArg", "--some"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });

        const lines = lineSplit(stdout);

        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), destinationBinary);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "FirstArg");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), "--some");
        assert.equal(lines[3], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[4])[1]), destinationBinary);

        // console.log(stdout);
        // console.warn(stderr);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [destinationBinary, "/snapshot/resource/javascript/loader.min.js", "FirstArg", "--some"]);
        assert.equal(normalize(data.cwd), cwd);
        assert.equal(normalize(data.dirname), "/snapshot/resource/javascript");
        assert.equal(normalize(data.filename), "/snapshot/resource/javascript/loader.min.js");
        if (data.env.hasOwnProperty("PWD")) {
            assert.equal(normalize(data.env.PWD), cwd);
        }
        assert.equal(data.env.OIDIS_NODEJS_DEBUG, "1");
        assert.equal(data.env.NODE_PATH, "/snapshot/node_modules");
        assert.ok(!data.env.OIDIS_NODEJS_MAIN_SKIP_PATH_RESOLVE);
        assert.ok(data.env.OIDIS_NODEJS_EMULATE_EMBEDDED);
        assert.ok(!data.env.OIDIS_NODEJS_SKIP_LOADER);
        assert.ok(!data.env.OIDIS_NODEJS_NODE_PATH);
        assert.ok(!data.env.OIDIS_NODEJS_NO_RESOURCE_MANAGER);
        assert.ok(!data.env.OIDIS_NODEJS_DEBUGGER_ATTACHED);

        const output = await execute(destinationBinary, [], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });
        // console.log(output.stdout);
        // console.warn(output.stderr);
        const lines2 = lineSplit(output.stdout);

        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines2[0])[1]), destinationBinary);
        assert.equal(lines2[1], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines2[2])[1]), destinationBinary);

        // produced by test script
        data = /^> run context:\s(.*)/gm.exec(output.stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [destinationBinary, "/snapshot/resource/javascript/loader.min.js"]);
        assert.equal(normalize(data.cwd), cwd);
        assert.equal(normalize(data.dirname), "/snapshot/resource/javascript");
        assert.equal(normalize(data.filename), "/snapshot/resource/javascript/loader.min.js");
        if (data.env.hasOwnProperty("PWD")) {
            assert.equal(normalize(data.env.PWD), cwd);
        }
        assert.equal(data.env.OIDIS_NODEJS_DEBUG, "1");
        assert.equal(data.env.NODE_PATH, "/snapshot/node_modules");
        assert.ok(!data.env.OIDIS_NODEJS_MAIN_SKIP_PATH_RESOLVE);
        assert.ok(data.env.OIDIS_NODEJS_EMULATE_EMBEDDED);
        assert.ok(!data.env.OIDIS_NODEJS_SKIP_LOADER);
        assert.ok(!data.env.OIDIS_NODEJS_NODE_PATH);
        assert.ok(!data.env.OIDIS_NODEJS_NO_RESOURCE_MANAGER);
        assert.ok(!data.env.OIDIS_NODEJS_DEBUGGER_ATTACHED);
    });

    describe("testLoaderWithInspect", async () => {
        const destinationBinary = createPackage(cwd + "/test.js");

        // run here?
        const {stdout, stderr} = await execute(destinationBinary, ["--inspect", "FirstArg", "--some"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });
        const lines = lineSplit(stdout);

        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), destinationBinary);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "--inspect");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), "FirstArg");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[3])[1]), "--some");
        assert.equal(lines[4], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[5])[1]), destinationBinary);

        // console.log(stdout);
        // console.warn(stderr);

        // dont know why this is in stderr
        assert.ok(stderr.match(/Debugger listening on ws:/g));

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [destinationBinary, "/snapshot/resource/javascript/loader.min.js", "FirstArg", "--some"]);
        assert.equal(normalize(data.cwd), cwd);
        assert.equal(normalize(data.dirname), "/snapshot/resource/javascript");
        assert.equal(normalize(data.filename), "/snapshot/resource/javascript/loader.min.js");
        if (data.env.hasOwnProperty("PWD")) {
            assert.equal(normalize(data.env.PWD), cwd);
        }
        assert.equal(data.env.OIDIS_NODEJS_DEBUG, "1");
        assert.equal(data.env.NODE_PATH, "/snapshot/node_modules");
        assert.ok(!data.env.OIDIS_NODEJS_MAIN_SKIP_PATH_RESOLVE);
        assert.ok(data.env.OIDIS_NODEJS_EMULATE_EMBEDDED);
        assert.ok(!data.env.OIDIS_NODEJS_SKIP_LOADER);
        assert.ok(!data.env.OIDIS_NODEJS_NODE_PATH);
        assert.ok(!data.env.OIDIS_NODEJS_NO_RESOURCE_MANAGER);
        assert.ok(data.env.OIDIS_NODEJS_DEBUGGER_ATTACHED);
    });

    describe("testLoaderVersionArg", async () => {
        const destinationBinary = createPackage(cwd + "/test.js");

        // run here?
        const {stdout, stderr} = await execute(destinationBinary, ["--version"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });
        const lines = lineSplit(stdout);

        assert.equal(stderr, "");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), destinationBinary);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "--version");
        assert.equal(lines[2], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), destinationBinary);
        assert.equal(lines[4], "v666.666.666");
    });

    describe("testLoaderHelpArg", async () => {
        const destinationBinary = createPackage(cwd + "/test.js");

        const {stdout, stderr} = await execute(destinationBinary, ["--help"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });
        const lines = lineSplit(stdout);

        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), destinationBinary);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "--help");
        assert.equal(lines[2], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), destinationBinary);
        assert.equal(lines[4], "THIS IS HELP OVERRIDE");
    });

    describe("testLoaderAnyDashArg", async () => {
        const destinationBinary = createPackage(cwd + "/test.js");

        const {stdout, stderr} = await execute(destinationBinary, ["--path"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });
        const lines = lineSplit(stdout);

        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), destinationBinary);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "--path");
        assert.equal(lines[2], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), destinationBinary);
        assert.equal(stderr, "");

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [destinationBinary, "/snapshot/resource/javascript/loader.min.js", "--path"]);
    });
});

suite(path.basename(__filename) + " - CJS - direct", async () => {
    setUp(() => {
        createShortContent(false, "", false);
    });

    tearDown(generalTearDown);

    describe("testCJSLoaderNoEmbedd_default", async () => {
        const testFile = cwd + "/test.js";
        const {stdout} = await execute(nodeBin, [testFile], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1
            }
        });

        const lines = lineSplit(stdout);

        // produced by internals
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), cwd + "/test.js");
        assert.equal(lines[2], "embedded: 0");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), nodeBin);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [nodeBin, testFile]);
        assert.equal(data.isCJS, true);
    });

    describe("testCJSLoaderNoEmbedd_cjs", async () => {
        const testFile = cwd + "/test.cjs";
        const {stdout} = await execute(nodeBin, [testFile], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1
            }
        });

        const lines = lineSplit(stdout);

        // produced by internals
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), cwd + "/test.cjs");
        assert.equal(lines[2], "embedded: 0");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), nodeBin);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [nodeBin, testFile]);
        assert.equal(data.isCJS, true);
    });

    describe("testCJSLoader_default", async () => {
        const destinationBinary = createPackage(cwd + "/test.js");

        // run here?
        const {stdout, stderr} = await execute(destinationBinary, ["FirstArg", "--some"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });

        const lines = lineSplit(stdout);

        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), destinationBinary);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "FirstArg");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), "--some");
        assert.equal(lines[3], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[4])[1]), destinationBinary);

        // console.log(stdout);
        // console.warn(stderr);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [destinationBinary, "/snapshot/resource/javascript/loader.min.js", "FirstArg", "--some"]);
        assert.equal(data.isCJS, true);
    });

    describe("testCJSLoader_cjs", async () => {
        const destinationBinary = createPackage(cwd + "/test.cjs");

        // run here?
        const {stdout, stderr} = await execute(destinationBinary, ["FirstArg", "--some"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });

        const lines = lineSplit(stdout);

        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), destinationBinary);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "FirstArg");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), "--some");
        assert.equal(lines[3], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[4])[1]), destinationBinary);

        // console.log(stdout);
        // console.warn(stderr);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [destinationBinary, "/snapshot/resource/javascript/loader.min.cjs", "FirstArg", "--some"]);
        assert.equal(data.isCJS, true);
    });

    describe("testCJSLoader_VersionArg", async () => {
        const destinationBinary = createPackage(cwd + "/test.cjs");

        // run here?
        const {stdout, stderr} = await execute(destinationBinary, ["--version"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });
        const lines = lineSplit(stdout);

        assert.equal(stderr, "");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), destinationBinary);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "--version");
        assert.equal(lines[2], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), destinationBinary);
        assert.equal(lines[4], "v666.666.666");
    });
});

suite(path.basename(__filename) + " - ESM - direct", async () => {
    setUp(() => {
        createShortContent(true, "", false);
    });

    tearDown(generalTearDown);

    describe("testESMLoaderNoEmbedd_mjs", async () => {
        const testFile = cwd + "/test.mjs";
        const {stdout, stderr} = await execute(nodeBin, [testFile], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1
            }
        });

        assert.equal(stderr, "");
        const lines = lineSplit(stdout);

        // produced by internals
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), cwd + "/test.mjs");
        assert.equal(lines[2], "embedded: 0");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), nodeBin);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [nodeBin, testFile]);
        assert.equal(data.isCJS, false);
    });

    describe("testESMLoader_default", async () => {
        const destinationBinary = createPackage(cwd + "/test.js");

        // run here?
        const {stdout, stderr} = await execute(destinationBinary, ["FirstArg", "--some"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });

        const lines = lineSplit(stdout);

        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), destinationBinary);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "FirstArg");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), "--some");
        assert.equal(lines[3], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[4])[1]), destinationBinary);

        // console.log(stdout);
        // console.warn(stderr);

        // produced by test script
        assert.ok(stderr.match(/SyntaxError: Cannot use import statement outside a module/g));
    });

    describe("testESMLoader_mjs", async () => {
        const destinationBinary = createPackage(cwd + "/test.mjs");

        // run here?
        const {stdout, stderr} = await execute(destinationBinary, ["FirstArg", "--some"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });

        const lines = lineSplit(stdout);

        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), destinationBinary);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "FirstArg");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), "--some");
        assert.equal(lines[3], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[4])[1]), destinationBinary);

        // console.log(stdout);
        // console.warn(stderr);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [destinationBinary, "/snapshot/resource/javascript/loader.min.mjs", "FirstArg", "--some"]);
        assert.equal(data.isCJS, false);
    });

    describe("testESMLoader_VersionArg", async () => {
        const destinationBinary = createPackage(cwd + "/test.mjs");

        // run here?
        const {stdout, stderr} = await execute(destinationBinary, ["--version"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });
        const lines = lineSplit(stdout);

        assert.equal(stderr, "");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), destinationBinary);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "--version");
        assert.equal(lines[2], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), destinationBinary);
        assert.equal(lines[4], "v666.666.666");
    });
});

suite(path.basename(__filename) + " - CJS - package.json", async () => {
    const cjsPath = cwd + "/testData";

    setUp(() => {
        createShortContent(false, cjsPath, true);
    });

    tearDown(generalTearDown);

    describe("testCJSLoaderNoEmbed", async () => {
        const testFile = cjsPath + "/test.js";
        const {stdout, stderr} = await execute(nodeBin, [testFile], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1
            }
        });

        const lines = lineSplit(stdout);

        assert.equal(stderr, "");
        // produced by internals
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), cjsPath + "/test.js");
        assert.equal(lines[2], "embedded: 0");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), nodeBin);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [nodeBin, testFile]);
        assert.equal(data.isCJS, true);
    });

    describe("testCJSLoader", async () => {
        const destinationBinary = createPackage(cjsPath + "/test.js");

        // run here?
        const {stdout, stderr} = await execute(destinationBinary, ["FirstArg", "--some"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });

        // console.log(stdout);
        // console.warn(stderr);

        const lines = lineSplit(stdout);

        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), destinationBinary);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "FirstArg");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), "--some");
        assert.equal(lines[3], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[4])[1]), destinationBinary);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [destinationBinary, "/snapshot/resource/javascript/loader.min.js", "FirstArg", "--some"]);
        assert.equal(data.isCJS, true);
    });
});

suite(path.basename(__filename) + " - ESM - package.json", async () => {
    const esmPath = cwd + "/testData";

    setUp(() => {
        createShortContent(true, esmPath, true);
    });

    tearDown(generalTearDown);

    describe("testESMLoaderNoEmbed", async () => {
        const testFile = esmPath + "/test.js";
        const {stdout, stderr} = await execute(nodeBin, [testFile], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1
            }
        });

        assert.equal(stderr, "");
        const lines = lineSplit(stdout);

        // produced by internals
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), nodeBin);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), esmPath + "/test.js");
        assert.equal(lines[2], "embedded: 0");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[3])[1]), nodeBin);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [nodeBin, testFile]);
        assert.equal(data.isCJS, false);
    });

    describe("testESMLoader", async () => {
        const destinationBinary = createPackage(esmPath + "/test.mjs");

        // run here?
        const {stdout, stderr} = await execute(destinationBinary, ["FirstArg", "--some"], {
            shell: true,
            windowsHide: true,
            env: {
                OIDIS_NODEJS_DEBUG: 1,
                OIDIS_NODEJS_EMULATE_EMBEDDED: 1
            }
        });

        const lines = lineSplit(stdout);

        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[0])[1]), destinationBinary);
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[1])[1]), "FirstArg");
        assert.equal(normalize(/item:\s+(.*)$/g.exec(lines[2])[1]), "--some");
        assert.equal(lines[3], "embedded: 1");
        assert.equal(normalize(/module:\s+(.*)$/g.exec(lines[4])[1]), destinationBinary);

        // console.log(stdout);
        // console.warn(stderr);

        // produced by test script
        let data = /^> run context:\s(.*)/gm.exec(stdout)[1];
        assert.ok(data.length > 50);
        data = JSON.parse((Buffer.from(data, "base64")).toString("utf8"));
        // console.log(data);

        assert.deepEqual(normalize(data.argv), [destinationBinary, "/snapshot/resource/javascript/loader.min.mjs", "FirstArg", "--some"]);
        assert.equal(data.isCJS, false);
    });
});

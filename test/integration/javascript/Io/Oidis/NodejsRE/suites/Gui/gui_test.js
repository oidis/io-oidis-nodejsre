/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

const {assert, describe, suite} = require("../../intern/testoid");

const path = require("path");
const os = require("os");
const gui = require("gui");

const isWin = (os.platform().toString() === "win32" || os.platform().toString() === "win64");
const isMac = (os.platform().toString() === "darwin");

const TaskbarProgressState = {
    NOPROGRESS: 0,
    INDETERMINATE: 1,
    NORMAL: 2,
    ERROR_STATE: 3,
    PAUSED: 4
};

function sleep(ms) {
    var start = new Date().getTime(), expire = start + ms;
    while (new Date().getTime() < expire) {
    }
    return;
}

const splashScreenSettings = {
    x: -1, y: -1,
    width: 456, height: 312,
    imagePath: "test/resource/BackgroundTransparent.png"
};

// const progressSettings = {
//     x: 2, y: 312-15-4,
//     width: 456-4, height: 15,
//     foreground:"#ff0000", background:"#00aaaa", visible:true, marque: false
// };

const progressSettings = {
    x: 2, y: 312 - 15 - 4,
    width: 456 - 4, height: 15,
    foreground: "test/resource/bar_fill.png", background: "test/resource/bar_border.png", visible: true, marque: false
};

const progressLabelSettings = {
    x: 50, y: 50,
    width: 150, height: 50, font: "", size: 33, color: "#ff0000", justify: 1
};

const statusLabelSettings = {
    x: 150, y: 100,
    width: 150, height: 50, font: "", size: 33, color: "#00ff00", justify: 0
};

suite(path.basename(__filename), () => {
    describe("testSplashScreenParams", () => {
        // test function, does nothing
        // assert.throws(
        //     () => {
        //         gui.dummy();
        //     }
        // );

        assert.throws(
            () => {
                gui.SplashScreenFactoryCreate();
                gui.Finalize();
            }
        );

        assert.throws(
            () => {
                gui.SplashScreenFactoryCreate(2);
                gui.Finalize();
            }
        );

        assert.doesNotThrow(
            () => {
                gui.SplashScreenFactoryCreate(splashScreenSettings);
                gui.Finalize();
            }
        );
    });

    describe("testSplashScreen", () => {
        assert.doesNotThrow(
            () => {
                gui.SplashScreenFactoryCreate(splashScreenSettings);
                gui.SplashScreenShow();
                gui.SplashScreenHide();
                gui.Finalize();
            }
        );

        assert.doesNotThrow(
            () => {
                gui.SplashScreenFactoryCreate(splashScreenSettings);
                gui.SplashScreenSetOnOpenCB(() => {
                    console.log("Splash screen opened");
                });
                gui.SplashScreenSetOnCloseCB(() => {
                    console.log("Splash screen closed");
                });
                gui.SplashScreenShow();
                gui.SplashScreenHide();
                gui.Finalize();
            }
        );

        assert.doesNotThrow(
            () => {
                gui.SplashScreenFactoryCreate(splashScreenSettings);
                gui.SplashScreenSetOnOpenCB(() => {
                    console.log("Splash screen opened");
                });
                gui.SplashScreenSetOnCloseCB(() => {
                    console.log("Splash screen closed");
                });
                gui.SplashScreenAddProgressBar(progressSettings);
                gui.SplashScreenAddProgressLabel(progressLabelSettings);
                gui.SplashScreenAddStatusLabel(statusLabelSettings);
                gui.SplashScreenShow();

                const simulate = ($val) => {
                    gui.SplashScreenSetProgressBarValue($val);
                    gui.SplashScreenSetProgressLabelValue("" + $val);
                    gui.SplashScreenSetStatusLabelValue("" + ($val * 4));
                    // console.log("progress value: " + $val);
                    gui.SplashScreenUpdate();
                    if ($val >= 125) {
                        gui.SplashScreenHide();
                        gui.Finalize();
                        return;
                    }
                    sleep(100);
                    simulate($val + 5);
                };
                simulate(0);
            }
        );
    });

    if (isWin || isMac) {
        describe("testTaskBar", () => {
            assert.doesNotThrow(
                () => {
                    gui.TaskBarFactoryCreate();
                    gui.Finalize();
                }
            );

            assert.doesNotThrow(
                () => {
                    gui.TaskBarFactoryCreate();
                    gui.TaskBarSetProgressValue(0, 100);
                    gui.TaskBarSetProgressValue(50, 100);
                    gui.TaskBarSetProgressValue(100, 100);
                    gui.Finalize();
                }
            );

            assert.doesNotThrow(
                () => {
                    gui.TaskBarFactoryCreate();
                    gui.TaskBarSetProgressState(TaskbarProgressState.NORMAL);
                    gui.TaskBarSetProgressState(TaskbarProgressState.ERROR_STATE);
                    gui.TaskBarSetProgressState(TaskbarProgressState.INDETERMINATE);
                    gui.TaskBarSetProgressState(TaskbarProgressState.NOPROGRESS);
                    gui.TaskBarSetProgressState(TaskbarProgressState.PAUSED);
                    gui.Finalize();
                }
            );
        });
    }
});

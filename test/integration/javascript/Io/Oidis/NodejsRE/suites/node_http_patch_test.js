/* ********************************************************************************************************* *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

const {assert, describe, setUp, tearDown, suite} = require("../intern/testoid");
const {execute, normalize, copyRecursiveSync, lineSplit} = require("../intern/utils");

const path = require("path");
const fs = require("fs");
const os = require("os");

const isWin = (os.platform().toString() === "win32" || os.platform().toString() === "win64");
const nodeBin = path.resolve(normalize(process.argv0));
const cwd = normalize(process.cwd());
const destFolder = cwd + "/build/target/test";

async function prepareNode() {
    const resourceManager = require("resource_manager");
    const sourceFolder = path.dirname(nodeBin);

    let destinationBinary = destFolder + "/node" + (isWin ? ".exe" : "");

    copyRecursiveSync(sourceFolder, destFolder);

    resourceManager.initialize(destinationBinary);
    const packageMap = {
        "/snapshot/resource/javascript/loader.min.js": {
            "name": "RES_PACKAGE_MAP",
            "stats": "{\"dev\":16777220,\"mode\":33188,\"uid\":501,\"gid\":20,\"ino\":108040448,\"size\":10941,\"atimeMs\":1614007935186.9988,\"mtimeMs\":1614007935187.1265,\"ctimeMs\":1614007935187.1265,\"atime\":\"2021-02-22T15:32:15.187Z\",\"mtime\":\"2021-02-22T15:32:15.187Z\",\"ctime\":\"2021-02-22T15:32:15.187Z\",\"birthtime\":\"2021-02-22T15:32:15.187Z\"}"
        }
    }
    fs.writeFileSync(cwd + "/packedMap.json", JSON.stringify(packageMap));
    const loaderMinJs = cwd + "/test.js";

    resourceManager.write("CONFIG", "PACKAGE_MAP", cwd + "/packedMap.json", function ($message, $verbosity) {
        //console.log($verbosity + ", " + $message);
    });
    resourceManager.write("RESOURCES", "RES_PACKAGE_MAP", loaderMinJs, function ($message, $verbosity) {
        //console.log($verbosity + ", " + $message);
    });
    return destinationBinary;
}

function parseOutput($stdout) {
    const lines = lineSplit($stdout);

    assert.equal(lines.length, 6, "> " + $stdout);
    assert.equal(lines[0], ">> invoke start");
    assert.equal(lines[1], "> hello from http");
    assert.equal(lines[3], "> hello from https");
    assert.equal(lines[5], ">> invoke stop");

    function parseLine($line) {
        const result = /https?-status:\s(\d+);\s(.*)/g.exec($line);
        assert.ok(result);
        assert.equal(result.length, 3);
        return {
            status: parseInt(result[1]),
            host: result[2]
        }
    }

    return {
        http: parseLine(lines[2]),
        https: parseLine(lines[4])
    };
}

if (process.env.hasOwnProperty("http_proxy") || process.env.hasOwnProperty("https_proxy") ||
    process.env.hasOwnProperty("HTTP_PROXY") || process.env.hasOwnProperty("HTTPS_PROXY")) {
    console.log("\nWARNING: node_http_patch_test suite disabled behind proxy until tests will be compliant with proxy\n");
    return;
}

suite(path.basename(__filename), () => {
    setUp(async () => {
        if (fs.existsSync(destFolder)) {
            fs.rmSync(destFolder, {maxRetries: 10, recursive: true});
        }
        fs.mkdirSync(destFolder, {recursive: true});

        function httpRequest($callback) {
            console.log('> hello from http');
            require("http").request({
                path: "hub.dev.oidis.io",
                method: "GET",
                port: 443,
                rejectUnauthorized: false,
                strictSSL: false
            }, ($response) => {
                let str = "";
                $response.on('data', function ($chunk) {
                    str += $chunk;
                });
                $response.on('end', () => {
                    console.log("http-status: " + $response.statusCode + "; " + $response.client._host);
                    $callback();
                })
            }).end();
        }

        function httpsRequest($callback) {
            console.log('> hello from https');
            require("https").request({
                path: "hub.oidis.io",
                method: "GET",
                port: 443,
                rejectUnauthorized: false,
                strictSSL: false
            }, ($response) => {
                let str = "";
                $response.on('data', function ($chunk) {
                    str += $chunk;
                });
                $response.on('end', () => {
                    console.log("https-status: " + $response.statusCode + "; " + $response.client._host);
                    $callback();
                })
            }).end();
        }

        function requestTest($callback) {
            httpRequest(() => {
                httpsRequest($callback);
            });
        }

        fs.writeFileSync(cwd + "/test.js", "" +
            httpRequest.toString() + "\n" +
            httpsRequest.toString() + "\n" +
            requestTest.toString() + "\n" +
            "console.log('>> invoke start');\n" +
            "requestTest(($status)=>{\n" +
            "console.log('>> invoke stop');\n" +
            "    process.exit(0);\n" +
            "});"
        );
    });

    tearDown(async () => {
        try {
            fs.rmSync(cwd + "/packedMap.json", {force: true});
            fs.rmSync(cwd + "/test.js", {force: true});
            fs.rmSync(cwd + "/prof-dir", {maxRetries: 10, recursive: true});
            fs.rmSync(destFolder, {maxRetries: 10, recursive: true});
        } catch (e) {
            // dummy
        }
    });

    describe("testHttpToHttpPatch", async () => {
        const {stdout, stderr} = await execute(nodeBin, [cwd + "/test.js"], {
            shell: true,
            windowsHide: true,
            env: {
                NODEJSRE_MOCK_SERVER_MAP: JSON.stringify({"http://hub.dev.oidis.io": "http://google.com/unknown"})
            }
        });

        if (stderr.match("connect ECONNREFUSED")) {
            console.log("\nWARNING: ECONNREFUSED is acceptable if test env run behind proxy or VPN\n");
        } else {
            assert.equal(stderr, "");
            assert.deepEqual(
                parseOutput(stdout),
                {
                    http: {status: 404, host: 'google.com'},
                    https: {status: 200, host: 'hub.oidis.io'}
                }
            )
        }
    });

    describe("testHttpToHttpsPatch", async () => {
        const {stdout, stderr} = await execute(nodeBin, [cwd + "/test.js"], {
            shell: true,
            windowsHide: true,
            env: {
                NODEJSRE_MOCK_SERVER_MAP: JSON.stringify({"http://hub.dev.oidis.io": "https://google.com/unknown"})
            }
        });

        if (stderr.match("connect ECONNREFUSED")) {
            console.log("\nWARNING: ECONNREFUSED is acceptable if test env run behind proxy or VPN\n");
        } else {
            assert.deepEqual(
                parseOutput(stdout),
                {
                    http: {status: 404, host: 'google.com'},
                    https: {status: 200, host: 'hub.oidis.io'}
                }
            )
        }
    });

    describe("testHttpsToHttpPatch", async () => {
        const {stdout, stderr} = await execute(nodeBin, [cwd + "/test.js"], {
            shell: true,
            windowsHide: true,
            env: {
                NODEJSRE_MOCK_SERVER_MAP: JSON.stringify({"https://hub.oidis.io/": "http://google.com/unknown"})
            }
        });

        if (stderr.match("connect ECONNREFUSED")) {
            console.log("\nWARNING: ECONNREFUSED is acceptable if test env run behind proxy or VPN\n");
        } else {
            assert.deepEqual(
                parseOutput(stdout),
                {
                    http: {status: 301, host: 'hub.dev.oidis.io'},
                    https: {status: 404, host: 'google.com'}
                }
            )
        }
    });

    describe("testHttpsToHttpsPatch", async () => {
        const {stdout, stderr} = await execute(nodeBin, [cwd + "/test.js"], {
            shell: true,
            windowsHide: true,
            env: {
                NODEJSRE_MOCK_SERVER_MAP: JSON.stringify({"https://hub.oidis.io/": "https://google.com/unknown"})
            }
        });

        if (stderr.match("connect ECONNREFUSED")) {
            console.log("\nWARNING: ECONNREFUSED is acceptable if test env run behind proxy or VPN\n");
        } else {
            assert.deepEqual(
                parseOutput(stdout),
                {
                    http: {status: 301, host: 'hub.dev.oidis.io'},
                    https: {status: 404, host: 'google.com'}
                }
            )
        }
    });

    describe("testBothTypesPatch", async () => {
        const {stdout, stderr} = await execute(nodeBin, [cwd + "/test.js"], {
            shell: true,
            windowsHide: true,
            env: {
                NODEJSRE_MOCK_SERVER_MAP: JSON.stringify({
                    "http://hub.dev.oidis.io": "http://hub.oidis.io/unknown",
                    "https://hub.oidis.io/": "https://hub.dev.oidis.io/unknown"
                })
            }
        });

        if (stderr.match("connect ECONNREFUSED")) {
            console.log("\nWARNING: ECONNREFUSED is acceptable if test env run behind proxy or VPN\n");
        } else {
            assert.deepEqual(
                parseOutput(stdout),
                {
                    http: {status: 301, host: 'hub.oidis.io'},
                    https: {status: 404, host: 'hub.dev.oidis.io'}
                }
            )
        }
    });

    describe("testHttpPatchEmbeddedNode", async () => {
        // todo(mkelnar) disabled for win it did not catch both request, not sure why
        const destinationBinary = await prepareNode();

        const {stdout, stderr} = await execute(destinationBinary, [], {
            shell: true,
            windowsHide: true,
            env: {
                NODEJSRE_MOCK_SERVER_MAP: JSON.stringify({
                    "http://hub.oidis.io": "http://unknow.somewere/unknown",
                    "https://hub.oidis.io/": "https://unknow.somewere/unknown"
                })
            }
        });
        if (stderr.match("connect ECONNREFUSED")) {
            console.log("\nWARNING: ECONNREFUSED is acceptable if test env run behind proxy or VPN\n");
        } else {
            assert.deepEqual(parseOutput(stdout),
                {
                    http: {
                        host: 'hub.dev.oidis.io',
                        status: 301
                    },
                    https: {
                        host: 'hub.oidis.io',
                        status: 200
                    }
                }
            )
        }
    });
});

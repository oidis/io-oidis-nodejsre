/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

const {assert, describe, setUp, tearDown, suite} = require("../../intern/testoid");
const logger = require("../../intern/logger.js");

const fs = require("fs");
const path = require("path");
const process = require("process");

const resourceManager = require("resource_manager");
const os = require("os");

const sourceBinary = process.argv[0];

const isWin = (os.platform().toString() === "win32" || os.platform().toString() === "win64");

let destinationBinary = "node-for-testing" + (isWin ? ".exe" : "");

function checkArtifacts() {
    setTimeout(() => {
        if (fs.statSync(destinationBinary).size <= fs.statSync(sourceBinary).size) {
            throw Error(destinationBinary + " should have bigger size than " + sourceBinary);
        }
    }, 1000);
}

suite(path.basename(__filename), () => {
    setUp(() => {
        fs.copyFileSync(sourceBinary, destinationBinary);
        fs.chmodSync(destinationBinary, "777");
        destinationBinary = path.resolve(destinationBinary);

        if (isWin) {
            fs.copyFileSync(path.dirname(sourceBinary) + "\\libnode.dll", path.dirname(destinationBinary) + "\\libnode.dll");
        }
        resourceManager.initialize(destinationBinary);
    });
    tearDown(() => {
        fs.unlinkSync(destinationBinary);
        if (isWin) {
            fs.unlinkSync(path.dirname(destinationBinary) + "\\libnode.dll");
        }
    });

    describe("testWriteAndReadSimple", () => {
        resourceManager.write("TEST", "script", __filename, logger.logCallback);
        const data = resourceManager.read("TEST", "script", true, logger.logCallback);
        assert.strictEqual(Buffer.compare(data, fs.readFileSync(__filename)), 0);

        checkArtifacts();
    });
    describe("testWriteAndReadComplex", () => {
        resourceManager.write("TEST", "script", __filename, logger.logCallback);
        resourceManager.write("TEST", "binary", __filename, logger.logCallback);

        assert.strictEqual(Buffer.compare(resourceManager.read("TEST", "script", true, logger.logCallback), fs.readFileSync(__filename)), 0);
        assert.strictEqual(Buffer.compare(resourceManager.read("TEST", "binary", logger.logCallback), fs.readFileSync(__filename)), 0);

        const packageConfPath = path.join(__dirname, '..', '..', '..', '..', '..', '..', '..', '..', 'package.conf.jsonp');

        resourceManager.write("TEST", "script", packageConfPath, logger.logCallback);
        assert.strictEqual(Buffer.compare(resourceManager.read("TEST", "script", true, logger.logCallback), fs.readFileSync(packageConfPath)), 0);

        resourceManager.write("TEST", "newResource", packageConfPath, logger.logCallback);
        assert.strictEqual(Buffer.compare(resourceManager.read("TEST", "newResource", true, logger.logCallback), fs.readFileSync(packageConfPath)), 0);
        // check previous resources
        assert.strictEqual(Buffer.compare(resourceManager.read("TEST", "binary", logger.logCallback), fs.readFileSync(__filename)), 0);

        checkArtifacts();
    });
    describe("testWriteAndReadPerformance", () => {
        if (require("os").platform() === "win32") {
            console.log("WARNING: performance test disabled for WIN because big data needs to be embedded by ResourceHacker instead.");
            resourceManager.write("TEST", "script", __filename, logger.logCallback);  // dummy write to comply with check in tearDown
            return;
        }
        const packageConfPath = path.join(__dirname, '..', '..', '..', '..', '..', '..', '..', '..', 'package.conf.jsonp');
        let startTime = (new Date()).getTime();
        for (let i = 0; i < 500; i++) {
            resourceManager.write("TEST", "script_" + i, packageConfPath);
        }
        for (let i = 0; i < 5; i++) {
            resourceManager.write("TEST", "binary_" + i, __filename);
        }
        console.log("Write duration: " + ((new Date()).getTime() - startTime).toString() + " ms");

        const scriptData = fs.readFileSync(packageConfPath);
        const binaryData = fs.readFileSync(__filename);

        startTime = (new Date()).getTime()
        assert.strictEqual(Buffer.compare(resourceManager.read("TEST", "script_0", true), scriptData), 0);
        console.log("Read-cache duration: " + ((new Date()).getTime() - startTime).toString() + " ms");

        startTime = (new Date()).getTime()
        for (let i = 0; i < 500; i++) {
            assert.strictEqual(Buffer.compare(resourceManager.read("TEST", "script_" + i), scriptData), 0);
        }
        for (let i = 0; i < 5; i++) {
            assert.strictEqual(Buffer.compare(resourceManager.read("TEST", "binary_" + i), binaryData), 0);
        }
        console.log("Read duration: " + ((new Date()).getTime() - startTime).toString() + " ms");

        checkArtifacts();
    });
});

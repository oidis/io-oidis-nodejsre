/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

const {assert, describe, suite} = require("../../intern/testoid");
const logger = require("../../intern/logger.js");

const path = require("path");
const resourceManager = require("resource_manager");

suite(path.basename(__filename), () => {
    describe("testWriteWithIncorrectRequiredParameters", () => {
        const undefinedRequiredParameters = {
            name: "TypeError",
            message: "Group, ID and path to resource parameters have to be defined and not null"
        };

        assert.throws(
            () => {
                resourceManager.write(null);
            },
            undefinedRequiredParameters
        );
        assert.throws(
            () => {
                resourceManager.write("");
            },
            undefinedRequiredParameters
        );
        assert.throws(
            () => {
                resourceManager.write(null, null);
            },
            undefinedRequiredParameters
        );
        assert.throws(
            () => {
                resourceManager.write("", null);
            },
            undefinedRequiredParameters
        );
        assert.throws(
            () => {
                resourceManager.write(null, "");
            },
            undefinedRequiredParameters
        );
        assert.throws(
            () => {
                resourceManager.write("", "", null);
            },
            undefinedRequiredParameters
        );
        assert.throws(
            () => {
                resourceManager.write("", "");
            },
            undefinedRequiredParameters
        );

        const incorrectParamValueError = {
            name: "Error",
            message: "At least one of Group or ID parameters and path to resources have to be not empty"
        };
        assert.throws(
            () => {
                resourceManager.write("", "", "");
            },
            incorrectParamValueError
        );
        assert.throws(
            () => {
                resourceManager.write("some", "", "");
            },
            incorrectParamValueError
        );
        assert.throws(
            () => {
                resourceManager.write("", "some", "");
            },
            incorrectParamValueError
        );
        assert.throws(
            () => {
                resourceManager.write("", "", "some");
            },
            incorrectParamValueError
        );
    });

    describe("testWriteWithIncorrectParamTypes", () => {
        const incorrectTypeOfArgError = {
            name: "TypeError",
            message: "Group, ID, and path to resource have to be strings"
        };

        assert.throws(
            () => {
                resourceManager.write("some-group", 999, "path-to-resource", null);
            },
            incorrectTypeOfArgError
        );
        assert.throws(
            () => {
                resourceManager.write(true, "some-id", "path-to-resource", null);
            },
            incorrectTypeOfArgError
        );
        assert.throws(
            () => {
                resourceManager.write("some-group", "some-id", 5.6, null);
            },
            incorrectTypeOfArgError
        );
        assert.throws(
            () => {
                resourceManager.write("some-group", "some-id", "path-to-resource", 44);
            },
            {
                name: "TypeError",
                message: "The optional 4th argument has to be a function"
            }
        );
    });

    describe("testWriteWithIncorrectParams", () => {
        assert.throws(
            () => {
                resourceManager.initialize("./unknownpath", logger.logCallback);
                resourceManager.write("foo", "boo", "path-to-resource", logger.logCallback);
            },
            {
                name: "Error",
                message: "File path-to-resource cannot be opened for reading"
            }
        );
    });
});

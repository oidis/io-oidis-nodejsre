/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

"use strict";

const {assert, describe, suite} = require("../../intern/testoid");
const logger = require("../../intern/logger.js");

const path = require("path");
const os = require("os");
const resourceManager = require("resource_manager");

const isWin = (os.platform().toString() === "win32" || os.platform().toString() === "win64");

suite(path.basename(__filename), () => {
    describe("testReadWithIncorrectRequiredParameters", () => {
        const undefinedRequiredParameters = {
            name: "TypeError",
            message: "Group and ID parameters have to be defined and not null"
        };

        assert.throws(
            () => {
                resourceManager.read(null);
            },
            undefinedRequiredParameters
        );
        assert.throws(
            () => {
                resourceManager.read("");
            },
            undefinedRequiredParameters
        );
        assert.throws(
            () => {
                resourceManager.read(null, null);
            },
            undefinedRequiredParameters
        );
        assert.throws(
            () => {
                resourceManager.read("", null);
            },
            undefinedRequiredParameters
        );
        assert.throws(
            () => {
                resourceManager.read(null, "");
            },
            undefinedRequiredParameters
        );

        const incorrectParamValueError = {
            name: "Error",
            message: "At least one of Group or ID parameters has to be not empty"
        };
        assert.throws(
            () => {
                resourceManager.read("", "");
            },
            incorrectParamValueError
        );
        assert.throws(
            () => {
                resourceManager.read("", "", "", "");
            },
            incorrectParamValueError
        );
    });

    describe("testReadWithIncorrectParamTypes", () => {
        const incorrectTypeOfArgError = {
            name: "TypeError",
            message: "Group and id have to be strings"
        };

        assert.throws(
            () => {
                resourceManager.read(null, "ID", "path");
            },
            {
                name: "TypeError",
                message: "Group and ID parameters have to be defined and not null"
            }
        );
        assert.throws(
            () => {
                resourceManager.read("some-group", 999, "path");
            },
            incorrectTypeOfArgError
        );
        assert.throws(
            () => {
                resourceManager.read("some-group", true, "path");
            },
            incorrectTypeOfArgError
        );
    });

    if (!isWin) {
        describe("testReadWithIncorrectParams", () => {
            assert.throws(
                () => {
                    resourceManager.initialize("./unknownpath", logger.logCallback);
                    resourceManager.read("foo", "boo", logger.logCallback);
                },
                {
                    name: "Error",
                    message: "Could not open file ./unknownpath for reading"
                }
            );
            assert.throws(
                () => {
                    resourceManager.initialize(__filename, logger.logCallback);
                    resourceManager.read("some-group", "some-id", logger.logCallback);
                },
                {
                    name: "Error",
                    message: "Failed to validate the magic sequence"
                }
            );
        });
    }
});

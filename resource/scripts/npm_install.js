/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Io.Oidis.Builder.Loader;
const LogIt = Io.Oidis.Commons.Utils.LogIt;
const EnvironmentHelper = Io.Oidis.Localhost.Utils.EnvironmentHelper;
const terminal = Loader.getInstance().getTerminal();
const filesystem = Loader.getInstance().getFileSystemHandler();

Process = function ($cwd, $args, $done) {
    if (!EnvironmentHelper.IsWindows()) {
        if (filesystem.Exists($cwd + "/node_modules") && filesystem.IsSymbolicLink($cwd + "/npm")) {
            LogIt.Info("NPM already exists. Install script skipped.");
            $done();
        } else {
            terminal.Spawn("mv", [$cwd + "/lib/node_modules", $cwd + "/node_modules"], $cwd, ($exitCode) => {
                if ($exitCode !== 0) {
                    LogIt.Error("Cannot move node_modules folder.");
                } else {
                    terminal.Spawn("ln", ["-s", "node_modules/npm/bin/npm-cli.js", $cwd + "/npm"], $cwd, ($exitCode) => {
                        if ($exitCode !== 0) {
                            LogIt.Error("Cannot install NPM.");
                        }
                        $done();
                    });
                }
            });
        }
    } else {
        LogIt.Info("NPM install is not required on Windows.");
        $done();
    }
};
